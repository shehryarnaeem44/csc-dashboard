const express = require('express');

const app = express();

app.use(express.static('./csc-dashboard'));

app.get('/*', (req, res) =>
    res.sendFile('index.html', {root: 'csc-dashboard'}),
);

app.listen(process.env.PORT || 8080);