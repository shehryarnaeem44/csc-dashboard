import { Route } from "@angular/compiler/src/core";
import { Injectable } from "@angular/core";
import { Router, RouterEvent, RoutesRecognized } from "@angular/router";
import { filter, pairwise } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class RouterStateService {
  private _previousUrl: string;
  constructor(private router: Router) {
    this.router.events
      .pipe(
        filter((evt: any) => evt instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((events: RoutesRecognized[]) => {
        this._previousUrl = events[0].urlAfterRedirects;
      });
  }

  public get previousUrl() {
    return this._previousUrl;
  }
}
