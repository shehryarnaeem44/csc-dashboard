import { Injectable } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";

@Injectable({
  providedIn: "root"
})
export class SpinnerService {
  constructor(private ngxSpinnerService: NgxSpinnerService) {}

  public show() {
    this.ngxSpinnerService.show();
  }

  public hide() {
    this.ngxSpinnerService.hide();
  }
}
