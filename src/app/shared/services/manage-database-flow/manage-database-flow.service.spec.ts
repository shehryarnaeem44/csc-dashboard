import { TestBed } from '@angular/core/testing';

import { ManageDatabaseFlowService } from './manage-database-flow.service';

describe('ManageDatabaseFlowService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageDatabaseFlowService = TestBed.get(ManageDatabaseFlowService);
    expect(service).toBeTruthy();
  });
});
