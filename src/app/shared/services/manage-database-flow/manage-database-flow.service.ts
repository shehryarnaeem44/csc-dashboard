import { Injectable } from "@angular/core";
import { Game } from "../../interfaces/game.interface";
import { Source } from "../../interfaces/source.interface";

@Injectable({
  providedIn: "root"
})
export class ManageDatabaseFlowService {
  private _game: Game;
  private _source: Source;

  constructor() {
    this._game = {} as Game;
    this._source = {} as Source;
  }

  public set game(game: Game) {
    this._game = game;
  }

  public get game() {
    return this._game;
  }

  public set source(source: Source) {
    this._source = source;
  }

  public get source() {
    return this._source;
  }

  public resetFlow() {
    this._game = {} as Game;
    this._source = {} as Source;
  }
}
