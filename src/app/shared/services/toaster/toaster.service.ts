import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root"
})
export class ToasterService {
  public constructor(private toasterService: ToastrService) {}

  public showSuccess(message?: string) {
    this.toasterService.success(message || "Successfull!");
  }

  public showError(message?: string) {
    this.toasterService.error(message || "Error!");
  }

  public showInfo(message: string) {
    this.toasterService.info(message);
  }

  public showWarning(message: string) {
    this.toasterService.warning(message);
  }
}
