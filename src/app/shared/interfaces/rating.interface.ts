export interface Rating {
  _id: number;
  shortName: string;
  longName: string;
  description?: string;
  imageUrl?: string;
  board: string;
}
