import { Common } from "./common.interface";

export interface Category extends Common{
    description:string;
}