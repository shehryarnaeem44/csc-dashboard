import { PlatformManufacturer } from "./platform-manufacturer.interface";

export interface Platform {
  _id?: Number;
  manufacturer?: string;
  generationId?: Number;
  shortName: String;
  longName: String;
  generation?: number;
  releaseDate: string;
}
