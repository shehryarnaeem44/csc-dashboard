import { Game } from "./game.interface";
import { Media } from "./media.interface";
import { Platform } from "./platform.interface";

export interface Source extends Media {
  _id?: string;
  name?: string;
  order?: number;
  game?: Game;
  author: string;
  createdDate?: Date;
  lastUpdated?: Date;
  airCount?: number;
  recordingPlatform?: Platform;
  gameId?: string;
  platformId?: string;
}
