export interface Media {
  duration: number;
  resolution: string;
  format: string;
  videoBitrate?: string;
  audioBitrate?: string;
  filePath: string;
}
