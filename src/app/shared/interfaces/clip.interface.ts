import { Category } from "./category.interface";
import { Game } from "./game.interface";
import { Media } from "./media.interface";
import { Source } from "./source.interface";

export interface Clip extends Media {
  _id?: string;
  name: string;
  order?:number;
  description: string;
  source?: Source;
  category?: Category;
  author: string;
  tags: string;
  lastUpdated?: Date;
  airCount?: number;
  sourceId?: string;
  categoryId?: string;
  excludeFromPlaylist?: boolean;
  game?: Game;
  isSelected?: boolean;
}
