import { Common } from "./common.interface";
import { Region } from "./region.interface";

export interface Developer extends Common {
  region?: Region;
  isSelected?: boolean;
  regionId?: string;
}
