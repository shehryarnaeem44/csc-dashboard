import { Component, EventEmitter, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { BsModalRef } from "ngx-bootstrap";
import { DevelopersService } from "src/app/core/http-services/developers/developers.service";
import { SOMETHING_WRONG } from "../../constants/general.constants";
import { Developer } from "../../interfaces/developer.interface";
import { SpinnerService } from "../../services/spinner/spinner.service";
import { ToasterService } from "../../services/toaster/toaster.service";

@Component({
  selector: "csc-add-developers-modal",
  templateUrl: "./add-developers-modal.component.html",
  styleUrls: ["./add-developers-modal.component.scss"]
})
export class AddDevelopersModalComponent implements OnInit {
  public developerForm: FormGroup;
  public isSubmitted: boolean;
  public event: EventEmitter<boolean> = new EventEmitter();
  constructor(
    private developerService: DevelopersService,
    private formBuilder: FormBuilder,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    private modalService: BsModalRef
  ) {
    this.isSubmitted = false;
  }

  ngOnInit() {
    this.buildDeveloperForm();
  }

  public buildDeveloperForm() {
    this.developerForm = this.formBuilder.group({
      name: [, [Validators.required]]
    });
  }

  public getControl(name: string): AbstractControl {
    return this.developerForm.get(name);
  }

  public async onSubmit(): Promise<void> {
    try {
      this.spinnerService.show();
      this.isSubmitted = true;
      const newDeveloper: Developer = {
        name: this.developerForm.get("name").value
      };
      await this.developerService.addDeveloper(newDeveloper);
      this.developerForm.reset();
      this.spinnerService.hide();
      this.onClose(true);
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
      this.onClose(false);
    }
  }

  triggerEvent(status: boolean) {
    this.event.emit(status || false);
  }

  public onClose(status?) {
    this.triggerEvent(status);
    this.modalService.hide();
  }
}
