import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDevelopersModalComponent } from './add-developers-modal.component';

describe('AddDevelopersModalComponent', () => {
  let component: AddDevelopersModalComponent;
  let fixture: ComponentFixture<AddDevelopersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDevelopersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDevelopersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
