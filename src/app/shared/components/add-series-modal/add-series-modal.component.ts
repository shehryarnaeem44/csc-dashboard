import { Component, EventEmitter, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { BsModalRef } from "ngx-bootstrap";
import { SeriesService } from "src/app/core/http-services/series/series.service";
import { SOMETHING_WRONG } from "../../constants/general.constants";
import { Series } from "../../interfaces/series.interface";
import { SpinnerService } from "../../services/spinner/spinner.service";
import { ToasterService } from "../../services/toaster/toaster.service";

@Component({
  selector: "csc-add-series-modal",
  templateUrl: "./add-series-modal.component.html",
  styleUrls: ["./add-series-modal.component.scss"]
})
export class AddSeriesModalComponent implements OnInit {
  public seriesForm: FormGroup;
  public isSubmitted: boolean;
  public event: EventEmitter<boolean> = new EventEmitter();
  constructor(
    private seriesService: SeriesService,
    private formBuilder: FormBuilder,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    private modalService: BsModalRef
  ) {
    this.isSubmitted = false;
  }

  ngOnInit() {
    this.buildSeriesForm();
  }

  public buildSeriesForm() {
    this.seriesForm = this.formBuilder.group({
      name: [, [Validators.required]]
    });
  }

  public getControl(name: string): AbstractControl {
    return this.seriesForm.get(name);
  }

  public async onSubmit(): Promise<void> {
    try {
      this.spinnerService.show();
      this.isSubmitted = true;
      const newSeries: Series = {
        name: this.seriesForm.get("name").value
      };
      await this.seriesService.addSeries(newSeries);
      this.seriesForm.reset();
      this.spinnerService.hide();
      this.onClose(true);
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
      this.onClose(false);
    }
  }

  triggerEvent(status: boolean) {
    this.event.emit(status || false);
  }

  public onClose(status?) {
    this.triggerEvent(status);
    this.modalService.hide();
  }
}
