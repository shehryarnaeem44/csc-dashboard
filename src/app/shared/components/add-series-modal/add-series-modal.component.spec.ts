import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSeriesModalComponent } from './add-series-modal.component';

describe('AddSeriesModalComponent', () => {
  let component: AddSeriesModalComponent;
  let fixture: ComponentFixture<AddSeriesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSeriesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSeriesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
