import { Component, Input, OnInit } from "@angular/core";
import { ThemeOptions } from "src/app/theme-options";

@Component({
  selector: "csc-page-title",
  templateUrl: "./page-title.component.html",
  styleUrls: ["./page-title.component.scss"],
})
export class PageTitleComponent implements OnInit {
  @Input() public titleHeading: string;
  @Input() public titleDescription: string;
  @Input() public newItemUrl: string;

  public constructor(public globals: ThemeOptions) {}

  public ngOnInit() {}
}
