import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFranchiseModalComponent } from './add-franchise-modal.component';

describe('AddFranchiseModalComponent', () => {
  let component: AddFranchiseModalComponent;
  let fixture: ComponentFixture<AddFranchiseModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFranchiseModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFranchiseModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
