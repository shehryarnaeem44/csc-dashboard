import { Component, EventEmitter, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { FranchiseService } from 'src/app/core/http-services/franchise/franchise.service';
import { SOMETHING_WRONG } from '../../constants/general.constants';
import { Franchise } from '../../interfaces/franchise.interface';
import { SpinnerService } from '../../services/spinner/spinner.service';
import { ToasterService } from '../../services/toaster/toaster.service';

@Component({
  selector: 'csc-add-franchise-modal',
  templateUrl: './add-franchise-modal.component.html',
  styleUrls: ['./add-franchise-modal.component.scss']
})
export class AddFranchiseModalComponent implements OnInit {
  public franchiseForm: FormGroup;
  public isSubmitted: boolean;
  public event: EventEmitter<boolean> = new EventEmitter();
  constructor(
    private franchiseService: FranchiseService,
    private formBuilder: FormBuilder,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    private modalService: BsModalRef
  ) {
    this.isSubmitted = false;
  }

  ngOnInit() {
    this.buildFranchiseForm();
  }

  public buildFranchiseForm() {
    this.franchiseForm = this.formBuilder.group({
      name: [, [Validators.required]]
    });
  }

  public getControl(name: string): AbstractControl {
    return this.franchiseForm.get(name);
  }

  public async onSubmit(): Promise<void> {
    try {
      this.spinnerService.show();
      this.isSubmitted = true;
      const newFranchise: Franchise = {
        name: this.franchiseForm.get("name").value
      };
      await this.franchiseService.addFranchise(newFranchise);
      this.franchiseForm.reset();
      this.spinnerService.hide();
      this.onClose(true);
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
      this.onClose(false);
    }
  }

  triggerEvent(status: boolean) {
    this.event.emit(status || false);
  }

  public onClose(status?) {
    this.triggerEvent(status);
    this.modalService.hide();
  }
}
