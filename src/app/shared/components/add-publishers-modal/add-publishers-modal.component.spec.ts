import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPublishersModalComponent } from './add-publishers-modal.component';

describe('AddPublishersModalComponent', () => {
  let component: AddPublishersModalComponent;
  let fixture: ComponentFixture<AddPublishersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPublishersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPublishersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
