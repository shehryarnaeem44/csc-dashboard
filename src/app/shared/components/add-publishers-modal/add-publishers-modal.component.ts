import { Component, EventEmitter, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { BsModalRef } from "ngx-bootstrap";
import { PublishersService } from "src/app/core/http-services/publishers/publishers.service";
import { SOMETHING_WRONG } from "../../constants/general.constants";
import { Publisher } from "../../interfaces/publisher.interface";
import { SpinnerService } from "../../services/spinner/spinner.service";
import { ToasterService } from "../../services/toaster/toaster.service";

@Component({
  selector: "csc-add-publishers-modal",
  templateUrl: "./add-publishers-modal.component.html",
  styleUrls: ["./add-publishers-modal.component.scss"]
})
export class AddPublishersModalComponent implements OnInit {
  public publisherForm: FormGroup;
  public isSubmitted: boolean;
  public event: EventEmitter<boolean> = new EventEmitter();
  constructor(
    private publisherService: PublishersService,
    private formBuilder: FormBuilder,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    private modalService: BsModalRef
  ) {
    this.isSubmitted = false;
  }

  ngOnInit() {
    this.buildPublisherForm();
  }

  public buildPublisherForm() {
    this.publisherForm = this.formBuilder.group({
      name: [, [Validators.required]]
    });
  }

  public getControl(name: string): AbstractControl {
    return this.publisherForm.get(name);
  }

  public async onSubmit(): Promise<void> {
    try {
      this.spinnerService.show();
      this.isSubmitted = true;
      const newPublisher: Publisher = {
        name: this.publisherForm.get("name").value
      };
      await this.publisherService.addPublisher(newPublisher);
      this.publisherForm.reset();
      this.spinnerService.hide();
      this.onClose(true);
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
      this.onClose(false);
    }
  }

  triggerEvent(status: boolean) {
    this.event.emit(status || false);
  }

  public onClose(status?) {
    this.triggerEvent(status);
    this.modalService.hide();
  }
}
