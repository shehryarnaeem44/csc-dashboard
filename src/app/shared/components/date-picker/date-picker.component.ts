import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input, OnInit } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { BsDatepickerConfig } from "ngx-bootstrap";

@Component({
  selector: "csc-date-picker",
  templateUrl: "./date-picker.component.html",
  styleUrls: ["./date-picker.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatePickerComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatePickerComponent implements OnInit, ControlValueAccessor {

  public constructor(
    private cdref: ChangeDetectorRef
  ) {}
  @Input()
  public minDate?: Date;

  @Input()
  public maxDate?: Date;

  @Input()
  public isYear?:boolean;

  public date?: Date;

  public bsConfig: Partial<BsDatepickerConfig>;

  public onChanged: any = () => {};
  public onTouched: any = () => {};

  public ngOnInit() {
    this.bsConfig = {
      minDate: this.minDate,
      maxDate: this.maxDate,
      isAnimated: true,
      isDisabled: true,
    };

    if(this.isYear){
      this.bsConfig.minMode = 'year';
      this.bsConfig.dateInputFormat = 'YYYY'
    }
  }

  public writeValue(date: Date): void {
    this.date = date;
  }

  public registerOnChange(fn: any): void {
    this.onChanged = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public setDisabledState?(isDisabled: boolean): void {}

  onDateChange(ev){
    this.onChanged(this.date);
    this.cdref.detectChanges();
  }
}
