import { Component, EventEmitter, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { BsModalRef } from "ngx-bootstrap";
import { RegionService } from "src/app/core/http-services/region/region.service";
import { SOMETHING_WRONG } from "../../constants/general.constants";
import { Region } from "../../interfaces/region.interface";
import { SpinnerService } from "../../services/spinner/spinner.service";
import { ToasterService } from "../../services/toaster/toaster.service";

@Component({
  selector: "csc-add-region-modal",
  templateUrl: "./add-region-modal.component.html",
  styleUrls: ["./add-region-modal.component.scss"]
})
export class AddRegionModalComponent implements OnInit {
  public regionForm: FormGroup;
  public isSubmitted: boolean;
  public event: EventEmitter<boolean> = new EventEmitter();
  constructor(
    private regionService: RegionService,
    private formBuilder: FormBuilder,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    private modalService: BsModalRef
  ) {
    this.isSubmitted = false;
  }

  ngOnInit() {
    this.buildRegionForm();
  }

  public buildRegionForm() {
    this.regionForm = this.formBuilder.group({
      name: [, [Validators.required]]
    });
  }

  public getControl(name: string): AbstractControl {
    return this.regionForm.get(name);
  }

  public async onSubmit(): Promise<void> {
    try {
      this.spinnerService.show();
      this.isSubmitted = true;
      const newRegion: Region = {
        name: this.regionForm.get("name").value
      };
      await this.regionService.addRegion(newRegion);
      this.regionForm.reset();
      this.spinnerService.hide();
      this.onClose(true);
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
      this.onClose(false);
    }
  }

  triggerEvent(status: boolean) {
    this.event.emit(status || false);
  }

  public onClose(status?) {
    this.triggerEvent(status);
    this.modalService.hide();
  }
}
