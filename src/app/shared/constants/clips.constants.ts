export const ADD_CLIP_SUCCESS: string = "Clip added successfully!";
export const CLIP_NOT_FOUND: string = "Clip not found!";
export const CLIP_DELETE_SUCCESS: string = "Clip deleted successfully!";
