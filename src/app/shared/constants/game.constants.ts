export const GAME_ADD_SUCCESS: string = "Game added successfully!";
export const GAME_NOT_FOUND: string = "Game not found!";

export const RESOLUTION_ENUM: string[] = ["1080p60", "1080p30", "4K"];
export const VIDEO_FORMAT_ENUM: string[] = ["h264", "h265"];

export const GAME_DELETE_SUCCESS: string = "Game deleted successfully!";
