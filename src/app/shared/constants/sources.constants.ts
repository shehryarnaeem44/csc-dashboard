export const ADD_SOURCE_SUCCESS: string = "Source added successfully!";
export const SOURCE_NOT_FOUND: string = "Source not found!";

export const SOURCE_DELETE_SUCCESS: string = "Source deleted successfully!";
