export const AIRCOUNT_HIGH_TO_LOW: string = "Aircount-High to Low";
export const AIRCOUNT_LOW_TO_HIGH: string = "Aircount-Low to High";
export const PLAYLIST_SORT_OPTIONS: string[] = [
  AIRCOUNT_HIGH_TO_LOW,
  AIRCOUNT_LOW_TO_HIGH
];
