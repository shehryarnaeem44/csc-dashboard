import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import {
  BsDatepickerModule,
  BsDropdownModule,
  TooltipModule
} from "ngx-bootstrap";
import { TagInputModule } from "ngx-chips";
import { DatePickerComponent } from "./components/date-picker/date-picker.component";
import { PageTitleComponent } from "./components/page-title/page-title.component";
import { TagInputComponent } from "./components/tag-input/tag-input.component";
import { ToastrModule } from "ngx-toastr";
import { AddSeriesModalComponent } from "./components/add-series-modal/add-series-modal.component";
import { AddFranchiseModalComponent } from "./components/add-franchise-modal/add-franchise-modal.component";
import { AddRatingModalComponent } from "./components/add-rating-modal/add-rating-modal.component";
import { AddRegionModalComponent } from "./components/add-region-modal/add-region-modal.component";
import { AddDevelopersModalComponent } from "./components/add-developers-modal/add-developers-modal.component";
import { AddPublishersModalComponent } from "./components/add-publishers-modal/add-publishers-modal.component";

@NgModule({
  declarations: [
    PageTitleComponent,
    DatePickerComponent,
    TagInputComponent,
    AddSeriesModalComponent,
    AddFranchiseModalComponent,
    AddRatingModalComponent,
    AddRegionModalComponent,
    AddDevelopersModalComponent,
    AddPublishersModalComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TagInputModule,
    ToastrModule.forRoot({
      closeButton: true,
      preventDuplicates: true
    })
  ],
  exports: [
    PageTitleComponent,
    DatePickerComponent,
    TagInputComponent,
    AddSeriesModalComponent,
    AddFranchiseModalComponent,
    AddRatingModalComponent,
    AddRegionModalComponent,
    AddDevelopersModalComponent,
    AddPublishersModalComponent
  ]
})
export class SharedModule {}
