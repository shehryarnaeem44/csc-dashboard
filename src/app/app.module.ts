import { CommonModule } from "@angular/common";
import { HttpClientModule} from "@angular/common/http";

import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { CoreModule } from "./core/core.module";
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PaginationModule } from "ngx-bootstrap";
import { NgxSpinnerModule } from "ngx-spinner";
import { AddPlatformModalComponent } from "./features/platforms/add-platform-modal/add-platform-modal.component";
import { AddDevelopersModalComponent } from "./shared/components/add-developers-modal/add-developers-modal.component";
import { AddFranchiseModalComponent } from "./shared/components/add-franchise-modal/add-franchise-modal.component";
import { AddPublishersModalComponent } from "./shared/components/add-publishers-modal/add-publishers-modal.component";
import { AddRatingModalComponent } from "./shared/components/add-rating-modal/add-rating-modal.component";
import { AddRegionModalComponent } from "./shared/components/add-region-modal/add-region-modal.component";
import { AddSeriesModalComponent } from "./shared/components/add-series-modal/add-series-modal.component";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    AppRoutingModule,
    CoreModule,
    BrowserAnimationsModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    PaginationModule.forRoot(),
    NgxSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[
    AddPlatformModalComponent,
    AddSeriesModalComponent,
    AddFranchiseModalComponent,
    AddRatingModalComponent,
    AddRegionModalComponent,
    AddDevelopersModalComponent,
    AddPublishersModalComponent
  ]
})
export class AppModule {
  public constructor() {
}
}
