import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "csc-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  public constructor(
    private router:Router
  ) {}

  public ngOnInit() {}

  public onDatabaseClick(){
    this.router.navigateByUrl('/games')
  }

  public onProduceShowClick(){
    this.router.navigateByUrl('/playlist')
  }
}
