import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DevelopersRoutingModule } from './developers-routing.module';
import { AddDeveloperModalComponent } from './add-developer-modal/add-developer-modal.component';


@NgModule({
  declarations: [AddDeveloperModalComponent],
  imports: [
    CommonModule,
    DevelopersRoutingModule
  ]
})
export class DevelopersModule { }
