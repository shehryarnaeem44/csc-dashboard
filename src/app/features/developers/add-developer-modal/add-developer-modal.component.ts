import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'csc-add-developer-modal',
  templateUrl: './add-developer-modal.component.html',
  styleUrls: ['./add-developer-modal.component.scss']
})
export class AddDeveloperModalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
