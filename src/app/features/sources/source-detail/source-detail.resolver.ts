import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot
} from "@angular/router";
import { SourcesService } from "src/app/core/http-services/sources/sources.service";
import { SOURCE_NOT_FOUND } from "src/app/shared/constants/sources.constants";
import { Source } from "src/app/shared/interfaces/source.interface";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";

@Injectable({
  providedIn: "root"
})
export class SourceDetailResolver implements Resolve<any> {
  constructor(
    private sourceService: SourcesService,
    private router: Router,
    private toasterService: ToasterService
  ) {}
  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    try {
      const sourceId: string = route.params.sourceId;
      const source: Source = await this.sourceService.getSource(sourceId);
      if (!source) {
        this.router.navigateByUrl("/sources");
        this.toasterService.showError(SOURCE_NOT_FOUND);
        return;
      }
      return source;
    } catch (err) {
      this.router.navigateByUrl("/sources");
      this.toasterService.showError(SOURCE_NOT_FOUND);
      return err;
    }
  }
}
