import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Source } from "src/app/shared/interfaces/source.interface";

@Component({
  selector: "csc-source-detail",
  templateUrl: "./source-detail.component.html",
  styleUrls: ["./source-detail.component.scss"]
})
export class SourceDetailComponent implements OnInit {
  public source: Source;
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.getSource();
  }

  public getSource() {
    const resolvedData = this.activatedRoute.snapshot.data.data;
    this.source = resolvedData;
  }

  public onSourceEdit() {
    this.router.navigateByUrl(`/sources/edit-source/${this.source._id}`);
  }
}
