import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { SourcesRoutingModule } from "./sources-routing.module";
import { SourceListingComponent } from './source-listing/source-listing.component';
import { AddEditSourceComponent } from './add-edit-source/add-edit-source.component';
import { SourceDetailComponent } from './source-detail/source-detail.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { BsDropdownModule, PaginationModule, TypeaheadModule } from "ngx-bootstrap";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { SharedModule } from "src/app/shared/shared.module";
import { NgSelectModule } from "@ng-select/ng-select";

@NgModule({
  declarations: [SourceListingComponent, AddEditSourceComponent, SourceDetailComponent],
  imports: [
    CommonModule,
    SourcesRoutingModule,
    SharedModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    BsDropdownModule.forRoot(),
    PaginationModule.forRoot(),
    TypeaheadModule.forRoot(),
    NgSelectModule
  ],
})
export class SourcesModule { }
