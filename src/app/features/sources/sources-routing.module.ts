import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddEditSourceComponent } from "./add-edit-source/add-edit-source.component";
import { AddSourceResolver } from "./add-edit-source/add-source.resolver";
import { SourceDetailComponent } from "./source-detail/source-detail.component";
import { SourceDetailResolver } from "./source-detail/source-detail.resolver";
import { SourceListingComponent } from "./source-listing/source-listing.component";
import { SourcesResolver } from "./source-listing/source.resolver";

const routes: Routes = [
  {
    path: "",
    component: SourceListingComponent,
    resolve: { data: SourcesResolver }
  },
  {
    path: "add-source",
    component: AddEditSourceComponent,
    resolve: { data: AddSourceResolver }
  },
  {
    path: "source-detail/:sourceId",
    component: SourceDetailComponent,
    resolve: { data: SourceDetailResolver }
  },
  {
    path: "edit-source/:sourceId",
    component: AddEditSourceComponent,
    resolve: { data: AddSourceResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SourcesRoutingModule {}
