import { Injectable } from "@angular/core";
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  NavigationEnd,
  Resolve,
  Router,
  RouterStateSnapshot
} from "@angular/router";
import { GamesService } from "src/app/core/http-services/games/games.service";
import { PlatformService } from "src/app/core/http-services/platform/platform.service";
import { SourcesService } from "src/app/core/http-services/sources/sources.service";
import { SOMETHING_WRONG } from "src/app/shared/constants/general.constants";
import { Game } from "src/app/shared/interfaces/game.interface";
import { Source } from "src/app/shared/interfaces/source.interface";
import { ManageDatabaseFlowService } from "src/app/shared/services/manage-database-flow/manage-database-flow.service";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";
import { filter, first, take } from 'rxjs/operators';
import { promisify } from "util";
import { RouterStateService } from "src/app/shared/services/router-state/router-state.service";
@Injectable({
  providedIn: "root"
})
export class AddSourceResolver implements Resolve<any> {
  public constructor(
    private gameService: GamesService,
    private platformService: PlatformService,
    private databaseFlowService: ManageDatabaseFlowService,
    private sourceService: SourcesService,
    private router: Router,
    private toasterService: ToasterService,
    private routerStateService: RouterStateService
  ) {}

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    try{
      const sourceId: string = route.params.sourceId;
      let data:any = await this.getData(sourceId);
      data = this.checkForDatabaseFlow(data);
      return data;
    }catch(err){
      this.toasterService.showError(SOMETHING_WRONG)
      this.router.navigateByUrl('/sources')
    }
  }

  private checkForDatabaseFlow(data: any) {
    const previousUrl = this.routerStateService.previousUrl;
    if(!previousUrl){
      return data;
    }
    if (previousUrl.includes('add-game') || previousUrl.includes('edit-game') && this.databaseFlowService.game) {
      data.game = this.databaseFlowService.game;
    }
    return data;
  }

  public getData(sourceId: string){
    const promises:any[] = [
      this.gameService.getGames(),
      this.platformService.getPlatforms(),
      this.sourceService.getAuthors()
    ]

    if(sourceId){
      promises.push(this.sourceService.getSource(sourceId))
    }

    return Promise.all(promises)
      .then(results => {
        return {
          games: results[0].games,
          platforms: results[1],
          authors: results[2],
          source: sourceId && results.length === 4 ? results[3] : null
        };
      })
  }

}
