import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditSourceComponent } from './add-edit-source.component';

describe('AddEditSourceComponent', () => {
  let component: AddEditSourceComponent;
  let fixture: ComponentFixture<AddEditSourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditSourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
