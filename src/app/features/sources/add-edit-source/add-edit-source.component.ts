import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { SourcesService } from "src/app/core/http-services/sources/sources.service";
import {
  RESOLUTION_ENUM,
  VIDEO_FORMAT_ENUM
} from "src/app/shared/constants/game.constants";
import { SOMETHING_WRONG } from "src/app/shared/constants/general.constants";
import { ADD_SOURCE_SUCCESS } from "src/app/shared/constants/sources.constants";
import { Game } from "src/app/shared/interfaces/game.interface";
import { Platform } from "src/app/shared/interfaces/platform.interface";
import { Source } from "src/app/shared/interfaces/source.interface";
import { ManageDatabaseFlowService } from "src/app/shared/services/manage-database-flow/manage-database-flow.service";
import { SpinnerService } from "src/app/shared/services/spinner/spinner.service";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";
import * as moment from 'moment';

@Component({
  selector: "csc-add-edit-source",
  templateUrl: "./add-edit-source.component.html",
  styleUrls: ["./add-edit-source.component.scss"]
})
export class AddEditSourceComponent implements OnInit {
  public games: Game[];
  public authors: string[];
  public platforms: Platform[];
  public sourceForm: FormGroup;
  public resolutions: string[];
  public formats: string[];
  public isSubmitted: Boolean;
  private newSource: Source;
  private editSource: Source;
  videoUrl: any;
  public durationInMin:number;


  public constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toasterService: ToasterService,
    private spinnerService: SpinnerService,
    private sourceService: SourcesService,
    private databaseFlowService: ManageDatabaseFlowService,
    private router: Router,
    private sanitizer: DomSanitizer
  ) {
    this.games = [];
    this.platforms = [];
    this.authors = [];
    this.resolutions = RESOLUTION_ENUM;
    this.formats = VIDEO_FORMAT_ENUM;
    this.isSubmitted = false;
  }

  public ngOnInit() {
    this.getData();
    this.buildSourceForm();
    this.addDefaultFormData();
    this.checkGameAdded();
  }

  public getData() {
    const resolvedData = this.activatedRoute.snapshot.data.data;
    this.games = resolvedData.games;
    this.platforms = resolvedData.platforms;
    this.authors = resolvedData.authors;
    this.editSource = resolvedData.source;
  }

  public buildSourceForm() {
    this.sourceForm = this.formBuilder.group({
      gameTitle: [, [Validators.required]],
      name:[,[Validators.required]],
      order:[],
      duration: [{value:0, disabled:true}, [Validators.required]],
      resolution: [, [Validators.required]],
      format: [, [Validators.required]],
      recordingPlatform: [, [Validators.required]],
      videoBitrate: [],
      audioBitrate: [],
      filePath: [, [Validators.required]],
      author: [, [Validators.required]]
    });
  }

  public getControl(name: string): AbstractControl {
    return this.sourceForm.get(name);
  }

  public addDefaultFormData() {
    if (!this.editSource) {
      return;
    }

    this.sourceForm.patchValue({name:this.editSource.name});
    this.sourceForm.patchValue({order:this.editSource.order});
    this.sourceForm.patchValue({ duration: this.editSource.duration });
    this.sourceForm.patchValue({ resolution: this.editSource.resolution });
    this.sourceForm.patchValue({ format: this.editSource.format });
    this.sourceForm.patchValue({
      recordingPlatform: this.editSource.recordingPlatform._id
    });
    this.sourceForm.patchValue({ videoBitrate: this.editSource.videoBitrate });
    this.sourceForm.patchValue({ audioBitrate: this.editSource.audioBitrate });
    this.sourceForm.patchValue({ filePath: this.editSource.filePath });
    this.sourceForm.patchValue({ author: this.editSource.author });
  }

  public checkGameAdded() {
    let gameId: string;
    if(this.editSource){
      gameId = this.editSource.game._id
    }

    if(this.databaseFlowService.game._id){
      gameId = this.databaseFlowService.game._id
    }
    this.sourceForm.patchValue({ gameTitle: gameId });
  }

  public async onSubmit(): Promise<void> {
    try {
      this.spinnerService.show();
      this.isSubmitted = true;

      this.newSource = {
        gameId: this.sourceForm.get("gameTitle").value,
        name:this.sourceForm.get('name').value,
        order: this.sourceForm.get('order').value,
        duration: this.durationInMin,
        resolution: this.sourceForm.get("resolution").value,
        format: this.sourceForm.get("format").value,
        platformId: this.sourceForm.get("recordingPlatform").value,
        videoBitrate: this.sourceForm.get("videoBitrate").value,
        audioBitrate: this.sourceForm.get("audioBitrate").value,
        filePath: this.sourceForm.get("filePath").value,
        author: this.sourceForm.get("author").value
      };

      console.log(this.newSource)
      if(this.editSource){
        this.newSource._id = this.editSource._id
        await this.sourceService.editSource(this.newSource)
      }else{
        await this.sourceService.addSource(this.newSource);
      }

      this.spinnerService.hide();
      this.toasterService.showSuccess(ADD_SOURCE_SUCCESS);
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async onAddClip() {
    await this.onSubmit()
    this.databaseFlowService.source = this.newSource;
    this.router.navigateByUrl("/clips/add-clip");
  }

  readVideoUrl(event: any) {
    const files = event.target.files;
    this.sourceForm.patchValue({name: files[0].name})
    if (files && files[0]) {
      this.videoUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(files[0]));
    }
  }

  getDuration(e) {
    const duration = e.target.duration;
    const formattedDuration = moment.utc(duration * 1000).format('HH:mm:ss').toString();
    const durationInMin = moment.duration(duration,'seconds').asMinutes()
    const finalDuration = Math.round((Number(durationInMin) + Number.EPSILON) * 100) / 100
    this.durationInMin = durationInMin;
    this.sourceForm.patchValue({duration:formattedDuration})
  }
}
