import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from "@angular/router";
import { SourcesService } from "src/app/core/http-services/sources/sources.service";
import { Source } from "src/app/shared/interfaces/source.interface";

@Injectable({
  providedIn: "root"
})
export class SourcesResolver implements Resolve<any> {
  public constructor(private sourcesService: SourcesService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.sourcesService
      .getSources()
      .then((sourceList: any) => {
        return {
          sources: sourceList.sources,
          count: sourceList.count
        };
      })
      .catch(err => {
        return err;
      });
  }
}
