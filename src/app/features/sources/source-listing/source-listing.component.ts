import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { SourcesService } from "src/app/core/http-services/sources/sources.service";
import { SOMETHING_WRONG } from "src/app/shared/constants/general.constants";
import { SOURCE_DELETE_SUCCESS } from "src/app/shared/constants/sources.constants";
import { Source } from "src/app/shared/interfaces/source.interface";
import { SpinnerService } from "src/app/shared/services/spinner/spinner.service";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";

@Component({
  selector: "csc-source-listing",
  templateUrl: "./source-listing.component.html",
  styleUrls: ["./source-listing.component.scss"]
})
export class SourceListingComponent implements OnInit {
  public sources: Source[];
  public keyword: string
  public count: number;
  public currentPage: number;

  constructor(public activatedRoute: ActivatedRoute, private toasterService:ToasterService, private spinnerService:SpinnerService, private sourceService:SourcesService) {
    this.sources = [];
    this.currentPage = 1;
  }

  ngOnInit() {
    this.getSources();
  }

  public getSources() {
    const resolvedData = this.activatedRoute.snapshot.data.data;
    this.sources = resolvedData.sources;
    this.count = resolvedData.count;
  }

  public async fetchSources(page?:number){
    const sourceList = await this.sourceService.getSources(page || 0);
    this.sources = sourceList.sources;
    this.count = sourceList.count;
  }

  public async onDelete(sourceId: string): Promise<void> {
    try {
      this.spinnerService.show();
      await this.sourceService.deleteSource(sourceId);
      await this.fetchSources();
      this.spinnerService.hide();
      this.toasterService.showSuccess(SOURCE_DELETE_SUCCESS);
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async onSearch(){
    try {
      this.spinnerService.show();
      if (this.keyword !== "") {
        const sourceList = await this.sourceService.searchSources(this.keyword);
        this.sources = sourceList.sources;
        this.count = sourceList.count;
      } else {
        await this.fetchSources();
      }
      this.spinnerService.hide();
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async pageChanged(event) {
    try {
      this.spinnerService.show();
      await this.fetchSources(event.page-1);
      this.spinnerService.hide();
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }
}
