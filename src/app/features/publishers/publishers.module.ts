import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublishersRoutingModule } from './publishers-routing.module';
import { AddPublisherModalComponent } from './add-publisher-modal/add-publisher-modal.component';


@NgModule({
  declarations: [AddPublisherModalComponent],
  imports: [
    CommonModule,
    PublishersRoutingModule
  ]
})
export class PublishersModule { }
