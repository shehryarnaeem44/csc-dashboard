import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'csc-add-publisher-modal',
  templateUrl: './add-publisher-modal.component.html',
  styleUrls: ['./add-publisher-modal.component.scss']
})
export class AddPublisherModalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
