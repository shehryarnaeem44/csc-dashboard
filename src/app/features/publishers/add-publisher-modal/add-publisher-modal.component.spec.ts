import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPublisherModalComponent } from './add-publisher-modal.component';

describe('AddPublisherModalComponent', () => {
  let component: AddPublisherModalComponent;
  let fixture: ComponentFixture<AddPublisherModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPublisherModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPublisherModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
