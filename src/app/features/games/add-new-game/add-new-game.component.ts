import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormArrayName,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { DevelopersService } from "src/app/core/http-services/developers/developers.service";
import { FranchiseService } from "src/app/core/http-services/franchise/franchise.service";
import { GamesService } from "src/app/core/http-services/games/games.service";
import { PlatformService } from "src/app/core/http-services/platform/platform.service";
import { PublishersService } from "src/app/core/http-services/publishers/publishers.service";
import { RegionService } from "src/app/core/http-services/region/region.service";
import { SeriesService } from "src/app/core/http-services/series/series.service";
import { AddDevelopersModalComponent } from "src/app/shared/components/add-developers-modal/add-developers-modal.component";
import { AddFranchiseModalComponent } from "src/app/shared/components/add-franchise-modal/add-franchise-modal.component";
import { AddPublishersModalComponent } from "src/app/shared/components/add-publishers-modal/add-publishers-modal.component";
import { AddRegionModalComponent } from "src/app/shared/components/add-region-modal/add-region-modal.component";
import { AddSeriesModalComponent } from "src/app/shared/components/add-series-modal/add-series-modal.component";
import { GAME_ADD_SUCCESS } from "src/app/shared/constants/game.constants";
import { SOMETHING_WRONG } from "src/app/shared/constants/general.constants";
import { Developer } from "src/app/shared/interfaces/developer.interface";
import { Franchise } from "src/app/shared/interfaces/franchise.interface";
import { Game } from "src/app/shared/interfaces/game.interface";
import { Platform } from "src/app/shared/interfaces/platform.interface";
import { Publisher } from "src/app/shared/interfaces/publisher.interface";
import { Rating } from "src/app/shared/interfaces/rating.interface";
import { Region } from "src/app/shared/interfaces/region.interface";
import { Series } from "src/app/shared/interfaces/series.interface";
import { ManageDatabaseFlowService } from "src/app/shared/services/manage-database-flow/manage-database-flow.service";
import { SpinnerService } from "src/app/shared/services/spinner/spinner.service";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";
import { AddPlatformModalComponent } from "../../platforms/add-platform-modal/add-platform-modal.component";

@Component({
  selector: "csc-add-new-game",
  templateUrl: "./add-new-game.component.html",
  styleUrls: ["./add-new-game.component.scss"]
})
export class AddNewGameComponent implements OnInit {
  public series: Series[];
  public platforms: Platform[];
  public franchises: Franchise[];
  public regions: Region[];
  public developers: Developer[];
  public publishers: Publisher[];
  public ratings: Rating[];
  public games: string[];

  public addGameForm: FormGroup;
  public isSubmitted: boolean;
  private newGame: Game;
  public isEdit: boolean;
  public gameId: string;
  public constructor(
    public activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,
    private toasterService: ToasterService,
    private gameService: GamesService,
    private spinnerService: SpinnerService,
    private databaseFlowService: ManageDatabaseFlowService,
    private router: Router,
    private seriesService: SeriesService,
    private franchiseService: FranchiseService,
    private regionService: RegionService,
    private modalService: BsModalService,
    private platformService: PlatformService,
    private developerService: DevelopersService,
    private publisherService: PublishersService
  ) {
    this.series = [];
    this.platforms = [];
    this.franchises = [];
    this.regions = [];
    this.developers = [];
    this.publishers = [];
    this.ratings = [];
    this.games = [];

    this.isSubmitted = false;
    this.isEdit = false;
    this.newGame = {} as Game;
  }

  public ngOnInit() {
    this.getData();
    this.buildAddGameForm();
    this.addDefaultFormData();
  }

  public getData(): void {
    const resolvedData = this.activatedRoute.snapshot.data.data;
    this.series = resolvedData.series;
    this.platforms = resolvedData.platforms;
    this.franchises = resolvedData.franchises;
    this.regions = resolvedData.regions;
    this.developers = resolvedData.developers;
    this.publishers = resolvedData.publishers;
    this.ratings = resolvedData.ratings;
    this.games = resolvedData.games;
    this.isEdit = resolvedData.isEdit;
    this.newGame = resolvedData.game;
    this.gameId = this.newGame._id;
  }

  public buildAddGameForm(): void {
    this.addGameForm = this.formBuilder.group({
      title: [, [Validators.required]],
      parentGame: [],
      sortTitle: [],
      series: [],
      franchise: [],
      platform: [, [Validators.required]],
      region: [, [Validators.required]],
      releaseDate: [],
      rating: [],
      developers: [],
      publishers: [],
      tags: []
    });
  }

  public addDefaultFormData() {
    if (!this.isEdit) {
      return;
    }

    this.addGameForm.patchValue({ parentGame: this.newGame.parentGame });
    this.addGameForm.patchValue({ title: this.newGame.title });
    this.addGameForm.patchValue({ sortTitle: this.newGame.sortTitle });
    this.addGameForm.patchValue({ series: this.newGame.series._id });
    this.addGameForm.patchValue({ franchise: this.newGame.franchise._id });
    this.addGameForm.patchValue({ platform: this.newGame.platform._id });
    this.addGameForm.patchValue({ region: this.newGame.region._id });
    this.addGameForm.patchValue({
      releaseDate: new Date(this.newGame.releaseDate)
    });
    this.addGameForm.patchValue({ rating: this.newGame.rating._id });
    this.addGameForm.patchValue({
      developers: this.newGame.developers.map(dev => dev._id)
    });
    this.addGameForm.patchValue({
      publishers: this.newGame.publishers.map(publisher => publisher._id)
    });
    this.addGameForm.patchValue({
      tags: this.newGame.tags.split(",")
    });
  }

  public getControl(name: string): AbstractControl {
    return this.addGameForm.get(name);
  }

  public async onSubmit(): Promise<void> {
    try {
      this.spinnerService.show();
      this.isSubmitted = true;
      this.newGame = {
        title: this.addGameForm.get("title").value,
        parentGame:
          this.addGameForm.get("parentGame").value ||
          this.addGameForm.get("title").value,
        sortTitle:
          this.addGameForm.get("sortTitle").value ||
          this.addGameForm.get("title").value,
        seriesId: this.addGameForm.get("series").value,
        franchiseId: this.addGameForm.get("franchise").value,
        platformId: this.addGameForm.get("platform").value,
        regionId: this.addGameForm.get("region").value,
        releaseDate: this.addGameForm.get("releaseDate").value,
        ratingId: this.addGameForm.get("rating").value,
        developerIds: this.addGameForm.get("developers").value,
        publisherIds: this.addGameForm.get("publishers").value
      };

      const tags: string[] = this.addGameForm.get("tags").value.map(tag => {
        return tag.label;
      });

      const tagString: string = tags.join(",");
      this.newGame.tags = tagString;

      if (this.isEdit) {
        this.newGame._id = this.gameId;
        await this.gameService.editGame(this.newGame);
      } else {
        await this.gameService.addGame(this.newGame);
      }
      this.addGameForm.reset();
      this.spinnerService.hide();
      this.toasterService.showSuccess(GAME_ADD_SUCCESS);
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async onAddSource() {
    await this.onSubmit();
    this.databaseFlowService.game = this.newGame;
    this.router.navigateByUrl("/sources/add-source");
  }

  public async onSeriesChange(series: Series) {
    try {
      if (series && series.name && !series._id) {
        this.spinnerService.show();
        await this.seriesService.addSeries(series);
        this.series = await this.seriesService.getSeries();
        this.addGameForm.get("series").reset();
        this.spinnerService.hide();
      }
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async onFranchiseChange(franchise: Franchise) {
    try {
      if (franchise && franchise.name && !franchise._id) {
        this.spinnerService.show();
        await this.franchiseService.addFranchise(franchise);
        this.franchises = await this.franchiseService.getFranchises();
        this.addGameForm.get("franchise").reset();
        this.spinnerService.hide();
      }
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async onRegionChange(region: Region) {
    try {
      if (region && region.name && !region._id) {
        this.spinnerService.show();
        await this.regionService.addRegion(region);
        this.regions = await this.regionService.getRegions();
        this.addGameForm.get("region").reset();
        this.spinnerService.hide();
      }
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async fetchPlatforms() {
    this.spinnerService.show();
    this.platforms = await this.platformService.getPlatforms();
    this.spinnerService.hide();
  }

  public async onAddPlatform() {
    let bsModalRef: BsModalRef = this.modalService.show(
      AddPlatformModalComponent
    );
    bsModalRef.content.event.subscribe(result => {
      if (result) {
        this.fetchPlatforms();
      }
    });
  }

  public async onDeveloperChange(dev: Developer) {
    try {
      if (dev && dev.name && !dev._id) {
        this.spinnerService.show();
        await this.developerService.addDeveloper(dev);
        this.developers = await this.developerService.getDevelopers();
        this.addGameForm.get("developers").reset();
        this.spinnerService.hide();
      }
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async onPublisherChange(pub: Publisher) {
    try {
      if (pub && pub.name && !pub._id) {
        this.spinnerService.show();
        await this.publisherService.addPublisher(pub);
        this.publishers = await this.publisherService.getPublishers();
        this.addGameForm.get("publishers").reset();
        this.spinnerService.hide();
      }
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async onAddSeries() {
    let bsModalRef: BsModalRef = this.modalService.show(
      AddSeriesModalComponent
    );
    bsModalRef.content.event.subscribe(result => {
      if (result) {
        this.fetchSeries();
      }
    });
  }

  public async fetchSeries() {
    this.spinnerService.show();
    this.series = await this.seriesService.getSeries();
    this.spinnerService.hide();
  }


  public async onAddFranchise() {
    let bsModalRef: BsModalRef = this.modalService.show(
      AddFranchiseModalComponent
    );
    bsModalRef.content.event.subscribe(result => {
      if (result) {
        this.fetchFranchise();
      }
    });
  }

  public async fetchFranchise() {
    this.spinnerService.show();
    this.franchises = await this.franchiseService.getFranchises();
    this.spinnerService.hide();
  }

  public async onAddRegion() {
    let bsModalRef: BsModalRef = this.modalService.show(
      AddRegionModalComponent
    );
    bsModalRef.content.event.subscribe(result => {
      if (result) {
        this.fetchRegions();
      }
    });
  }

  public async fetchRegions() {
    this.spinnerService.show();
    this.regions = await this.regionService.getRegions();
    this.spinnerService.hide();
  }

  public async onAddDeveloper() {
    let bsModalRef: BsModalRef = this.modalService.show(
      AddDevelopersModalComponent
    );
    bsModalRef.content.event.subscribe(result => {
      if (result) {
        this.fetchDevelopers();
      }
    });
  }

  public async fetchDevelopers() {
    this.spinnerService.show();
    this.developers = await this.developerService.getDevelopers();
    this.spinnerService.hide();
  }

  public async onAddPublisher() {
    let bsModalRef: BsModalRef = this.modalService.show(
      AddPublishersModalComponent
    );
    bsModalRef.content.event.subscribe(result => {
      if (result) {
        this.fetchPublishers();
      }
    });
  }

  public async fetchPublishers() {
    this.spinnerService.show();
    this.publishers = await this.publisherService.getPublishers();
    this.spinnerService.hide();
  }
}
