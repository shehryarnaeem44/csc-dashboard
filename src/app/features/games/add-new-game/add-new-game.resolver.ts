import { Injectable } from "@angular/core";
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot,
  UrlSegment
} from "@angular/router";
import { promise } from "protractor";
import { DevelopersService } from "src/app/core/http-services/developers/developers.service";
import { FranchiseService } from "src/app/core/http-services/franchise/franchise.service";
import { GamesService } from "src/app/core/http-services/games/games.service";
import { PlatformService } from "src/app/core/http-services/platform/platform.service";
import { PublishersService } from "src/app/core/http-services/publishers/publishers.service";
import { RatingsService } from "src/app/core/http-services/ratings/ratings.service";
import { RegionService } from "src/app/core/http-services/region/region.service";
import { SeriesService } from "src/app/core/http-services/series/series.service";
import { Game } from "src/app/shared/interfaces/game.interface";

@Injectable({
  providedIn: "root"
})
export class AddNewGameResolver implements Resolve<any> {
  public constructor(
    public seriesService: SeriesService,
    public franchiseService: FranchiseService,
    public platformService: PlatformService,
    public regionService: RegionService,
    public developerService: DevelopersService,
    public publisherService: PublishersService,
    public ratingService: RatingsService,
    private gameService: GamesService,
    private router: Router
  ) {}

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    try{
      const gameId:string = route.params.gameId
      const url:string = route.url.toString();
      this.editCheck(url, gameId)
      const data = this.getData(gameId);
      return data
    }catch(err){
      return err
    }
  }

  public getData(gameId){
    const promises:any[] = [
      this.seriesService.getSeries(),
      this.franchiseService.getFranchises(),
      this.platformService.getPlatforms(),
      this.regionService.getRegions(),
      this.developerService.getDevelopers(),
      this.publisherService.getPublishers(),
      this.ratingService.getRatings(),
      this.gameService.getAllGames()
    ]

    if(gameId){
      promises.push(this.getGame(gameId))
    }
    return Promise.all(promises)
      .then((results: any) => {
        return {
          series: results[0],
          franchises: results[1],
          platforms: results[2],
          regions: results[3],
          developers: results[4],
          publishers: results[5],
          ratings: results[6],
          games:results[7],
          isEdit: gameId? true:false,
          game: gameId ?results[8]:{}
        };
      })

  }

  public async editCheck(url:string, gameId:string){
    if(url.includes('add-game')){
      return
    }
    else if(url.includes('edit-game') && gameId){
      return
    }else{
      this.router.navigateByUrl('/games')
    }
  }

  public async getGame(gameId){
    let game:Game = await this.gameService.getGame(gameId)
    if(!game){
      this.router.navigateByUrl('/games')
    }
    return game
  }
}
