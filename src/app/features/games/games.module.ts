import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { BsDropdownModule, PaginationModule, TypeaheadModule } from "ngx-bootstrap";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { SharedModule } from "src/app/shared/shared.module";
import { AddNewGameComponent } from "./add-new-game/add-new-game.component";
import { GamesListingComponent } from "./games-listing/games-listing.component";
import { GamesRoutingModule } from "./games-routing.module";
import { GameDetailComponent } from './game-detail/game-detail.component';
import { NgSelectModule } from "@ng-select/ng-select";
import { PlatformsModule } from "../platforms/platforms.module";
import { AddPlatformModalComponent } from "../platforms/add-platform-modal/add-platform-modal.component";

@NgModule({
  declarations: [GamesListingComponent, AddNewGameComponent, GameDetailComponent],
  imports: [
    CommonModule,
    GamesRoutingModule,
    SharedModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    BsDropdownModule.forRoot(),
    PaginationModule.forRoot(),
    TypeaheadModule.forRoot(),
    NgSelectModule,
    PlatformsModule
  ],
  entryComponents:[
    AddPlatformModalComponent
  ]
})
export class GamesModule { }
