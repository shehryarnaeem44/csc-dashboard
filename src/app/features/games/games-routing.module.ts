import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddNewGameComponent } from "./add-new-game/add-new-game.component";
import { AddNewGameResolver } from "./add-new-game/add-new-game.resolver";
import { GameDetailComponent } from "./game-detail/game-detail.component";
import { GameDetailResolver } from "./game-detail/game-detail.resolver";
import { GamesListingComponent } from "./games-listing/games-listing.component";
import { GamesResolver } from "./games-listing/games.resolver";

const routes: Routes = [
  {
    path: "",
    component: GamesListingComponent,
    resolve: { data: GamesResolver }
  },
  {
    path: "add-game",
    component: AddNewGameComponent,
    resolve: { data: AddNewGameResolver }
  },
  {
    path: "game-detail/:gameId",
    component: GameDetailComponent,
    resolve: { data: GameDetailResolver }
  },
  {
    path: "edit-game/:gameId",
    component: AddNewGameComponent,
    resolve: { data: AddNewGameResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GamesRoutingModule {}
