import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { GamesService } from "src/app/core/http-services/games/games.service";
import { GAME_DELETE_SUCCESS } from "src/app/shared/constants/game.constants";
import { SOMETHING_WRONG } from "src/app/shared/constants/general.constants";
import { Game } from "src/app/shared/interfaces/game.interface";
import { SpinnerService } from "src/app/shared/services/spinner/spinner.service";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";

@Component({
  selector: "csc-games-listing",
  templateUrl: "./games-listing.component.html",
  styleUrls: ["./games-listing.component.scss"]
})
export class GamesListingComponent implements OnInit {
  public games: Game[];
  public keyword: string;
  public currentPage: number;
  public count: number;

  public constructor(
    public activatedRoute: ActivatedRoute,
    private gameService: GamesService,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService
  ) {
    this.games = [];
    this.currentPage = 1;
  }

  public ngOnInit() {
    this.getGames();
  }

  public getGames(): void {
    const resolvedData = this.activatedRoute.snapshot.data.data;
    this.games = resolvedData.games;
    this.count = resolvedData.count;
  }

  public async fetchGames(page?: number): Promise<void> {
    const gameList = await this.gameService.getGames(page || 0);
    this.games = gameList.games;
    this.count = gameList.count;
  }

  public async onDelete(gameId: string): Promise<void> {
    try {
      this.spinnerService.show();
      await this.gameService.deleteGame(gameId);
      await this.fetchGames();
      this.spinnerService.hide();
      this.toasterService.showSuccess(GAME_DELETE_SUCCESS);
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async onSearch() {
    try {
      this.spinnerService.show();
      if (this.keyword !== "") {
        const gamesList = await this.gameService.searchGames(this.keyword)
        this.games = gamesList.games || [];
        this.count = gamesList.count;
      } else {
        await this.fetchGames();
      }
      this.spinnerService.hide();
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async pageChanged(event) {
    try {
      this.spinnerService.show();
      await this.fetchGames(event.page-1);
      this.spinnerService.hide();
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }
}
