import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from "@angular/router";
import { GamesService } from "src/app/core/http-services/games/games.service";
import { Game } from "src/app/shared/interfaces/game.interface";

@Injectable({
  providedIn: "root"
})
export class GamesResolver implements Resolve<any> {
  public constructor(private gamesService: GamesService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.gamesService
      .getGames()
      .then((gamesList: any) => {
        const games:Game[] = gamesList.games;
        const count:number =  parseInt(gamesList.count);
        return {games, count};
      })
      .catch(err => {
        return err;
      });
  }
}
