import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Game } from "src/app/shared/interfaces/game.interface";

@Component({
  selector: "csc-game-detail",
  templateUrl: "./game-detail.component.html",
  styleUrls: ["./game-detail.component.scss"]
})
export class GameDetailComponent implements OnInit {
  public game: Game;
  public constructor(
    public activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  public ngOnInit() {
    this.getGame();
  }

  public getGame() {
    this.game = this.activatedRoute.snapshot.data.data;
  }

  public onGameEdit(){
    this.router.navigateByUrl(`/games/edit-game/${this.game._id}`)
  }
}
