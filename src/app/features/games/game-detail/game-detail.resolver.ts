import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot
} from "@angular/router";
import { GamesService } from "src/app/core/http-services/games/games.service";
import { GAME_NOT_FOUND } from "src/app/shared/constants/game.constants";
import { Game } from "src/app/shared/interfaces/game.interface";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";

@Injectable({
  providedIn: "root"
})
export class GameDetailResolver implements Resolve<any> {
  public constructor(
    private gameService: GamesService,
    private router: Router,
    private toasterService: ToasterService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const gameId: string = route.params.gameId;
    return this.gameService
      .getGame(gameId)
      .then((game: Game) => {
        if (!game) {
          this.router.navigateByUrl("/games");
          this.toasterService.showError(GAME_NOT_FOUND);
          return;
        }
        return game;
      })
      .catch(err => {
        this.router.navigateByUrl("/games");
        this.toasterService.showError(GAME_NOT_FOUND);
        return err;
      });
  }
}
