import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { PlaylistService } from "src/app/core/http-services/playlist/playlist.service";
import { RESOLUTION_ENUM } from "src/app/shared/constants/game.constants";
import { SOMETHING_WRONG } from "src/app/shared/constants/general.constants";
import { PLAYLIST_SORT_OPTIONS } from "src/app/shared/constants/playlist.constants";
import { Category } from "src/app/shared/interfaces/category.interface";
import { Clip } from "src/app/shared/interfaces/clip.interface";
import { Developer } from "src/app/shared/interfaces/developer.interface";
import { Franchise } from "src/app/shared/interfaces/franchise.interface";
import { Game } from "src/app/shared/interfaces/game.interface";
import { Platform } from "src/app/shared/interfaces/platform.interface";
import { Publisher } from "src/app/shared/interfaces/publisher.interface";
import { Region } from "src/app/shared/interfaces/region.interface";
import { Series } from "src/app/shared/interfaces/series.interface";
import { SpinnerService } from "src/app/shared/services/spinner/spinner.service";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";
import * as FileSaver from 'file-saver';

@Component({
  selector: "csc-new-playlist",
  templateUrl: "./new-playlist.component.html",
  styleUrls: ["./new-playlist.component.scss"]
})
export class NewPlaylistComponent implements OnInit {
  public newPlaylistForm: FormGroup;
  public games: Game[];
  public series: Series[];
  public resolutions: string[];
  public regions: Region[];
  public platforms: Platform[];
  public developers: Developer[];
  public publishers: Publisher[];
  public franchises: Franchise[];
  public clips: Clip[];
  public sortOptions: string[];
  public categories: Category[];
  public filters:any;

  public isSubmitted: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toasterService: ToasterService,
    private spinnerService: SpinnerService,
    private playlistService: PlaylistService,
  ) {
    this.games = [];
    this.series = [];
    this.resolutions = RESOLUTION_ENUM;
    this.regions = [];
    this.platforms = [];
    this.developers = [];
    this.publishers = [];
    this.franchises = [];
    this.clips = [];
    this.categories = [];
    this.sortOptions = PLAYLIST_SORT_OPTIONS;
    this.isSubmitted = false;
  }

  ngOnInit() {
    this.getData();
    this.buildNewPlaylistForm();
  }

  public getData() {
    const resolvedData = this.activatedRoute.snapshot.data.data;
    this.games = resolvedData.games;
    this.series = resolvedData.series;
    this.regions = resolvedData.regions;
    this.platforms = resolvedData.platforms;
    this.developers = resolvedData.developers;
    this.publishers = resolvedData.publishers;
    this.franchises = resolvedData.franchises;
    this.categories = resolvedData.categories;
  }

  public buildNewPlaylistForm() {
    this.newPlaylistForm = this.formBuilder.group({
      programDuration: [60, [Validators.required]],
      minClipLength: [0.1, [Validators.required]],
      minClipRes: [],
      region: [],
      minGeneration: [],
      maxGeneration: [],
      platform: [],
      minReleaseYear: [],
      maxReleaseYear: [],
      developer: [],
      publisher: [],
      franchise: [],
      series: [],
      game: [],
      sortOption: [],
      included_categories: [],
      excluded_categories:[],
      included_tags:[],
      excluded_tags:[],
      excludedClips: []
    });
  }

  public getControl(name: string): AbstractControl {
    return this.newPlaylistForm.get(name);
  }

  public async onSubmit() {
    try {
      this.isSubmitted = true;
      this.spinnerService.show();
      this.filters = {
        programDuration:  this.newPlaylistForm.get("programDuration").value,
        minClipLength: this.newPlaylistForm.get("minClipLength").value,
        clipResolutions: this.newPlaylistForm.get("minClipRes").value,
        region: this.newPlaylistForm.get("region").value,
        minGeneration: this.newPlaylistForm.get("minGeneration").value,
        maxGeneration: this.newPlaylistForm.get("maxGeneration").value,
        platform: this.newPlaylistForm.get("platform").value,
        minReleaseYear: this.newPlaylistForm.get("minReleaseYear").value,
        maxReleaseYear: this.newPlaylistForm.get("maxReleaseYear").value,
        developers: this.newPlaylistForm.get("developer").value,
        publishers:this.newPlaylistForm.get("publisher").value,
        franchise: this.newPlaylistForm.get("franchise").value,
        series: this.newPlaylistForm.get("series").value,
        game: this.newPlaylistForm.get("game").value,
        included_categories: this.newPlaylistForm.get("included_categories").value,
        excluded_categories: this.newPlaylistForm.get("excluded_categories").value,
        excludedClips: this.newPlaylistForm.get('excludedClips').value
      };
      if (this.newPlaylistForm.get("included_tags").value) {
        const tags: string[] = this.newPlaylistForm
          .get("included_tags")
          .value.map(tag => {
            return tag.label;
          });

        const tagString: string = tags.join(",");
        this.filters.included_tags = tagString;
      }

      if (this.newPlaylistForm.get("excluded_tags").value) {
        const tags: string[] = this.newPlaylistForm
          .get("excluded_tags")
          .value.map(tag => {
            return tag.label;
          });

        const tagString: string = tags.join(",");
        this.filters.excluded_tags = tagString;
      }

      console.log(this.filters);
      this.clips = await this.playlistService.generatePlaylist(this.filters);
      this.spinnerService.hide();
    } catch (err) {
      console.log(err);
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async OnGenerateXmlPlaylist(){
    this.spinnerService.show()
    const selectedClips: Clip[] = this.clips.filter((clip)=>clip.isSelected)
    const playlist = {
      title: "playlist",
      clips: selectedClips
    }
    const xmlResponse = await this.playlistService.getXmlPlaylist(playlist);
    const xmlBlob = new Blob([xmlResponse.xml],{type: "text/plain;charset=utf-8"})
    FileSaver.saveAs(xmlBlob,"playlist.xspf");
    this.clips = await this.playlistService.generatePlaylist(this.filters);
    this.spinnerService.hide()
  }
}
