import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from "@angular/router";
import { CategoriesService } from "src/app/core/http-services/categories/categories.service";
import { DevelopersService } from "src/app/core/http-services/developers/developers.service";
import { FranchiseService } from "src/app/core/http-services/franchise/franchise.service";
import { GamesService } from "src/app/core/http-services/games/games.service";
import { PlatformService } from "src/app/core/http-services/platform/platform.service";
import { PublishersService } from "src/app/core/http-services/publishers/publishers.service";
import { RegionService } from "src/app/core/http-services/region/region.service";
import { SeriesService } from "src/app/core/http-services/series/series.service";
import { Category } from "src/app/shared/interfaces/category.interface";
import { Developer } from "src/app/shared/interfaces/developer.interface";
import { Franchise } from "src/app/shared/interfaces/franchise.interface";
import { Game } from "src/app/shared/interfaces/game.interface";
import { Platform } from "src/app/shared/interfaces/platform.interface";
import { Publisher } from "src/app/shared/interfaces/publisher.interface";
import { Region } from "src/app/shared/interfaces/region.interface";
import { Series } from "src/app/shared/interfaces/series.interface";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";

@Injectable({
  providedIn: "root"
})
export class NewPlaylistResolver implements Resolve<any> {
  constructor(
    private gameService: GamesService,
    private regionService: RegionService,
    private platformService: PlatformService,
    private developerService: DevelopersService,
    private publisherService: PublishersService,
    private franchiseService: FranchiseService,
    private seriesService: SeriesService,
    private categoryService: CategoriesService,
    private toasterService: ToasterService
  ) {}

  public async resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    try {
      const games: any = await this.gameService.getGames();
      const regions: Region[] = await this.regionService.getRegions();
      const platforms: Platform[] = await this.platformService.getPlatforms();
      const developers: Developer[] = await this.developerService.getDevelopers();
      const publishers: Publisher[] = await this.publisherService.getPublishers();
      const franchises: Franchise[] = await this.franchiseService.getFranchises();
      const series: Series[] = await this.seriesService.getSeries();
      const categories: Category[] = await this.categoryService.getCategories();

      return {
        games:games.games as Game[],
        regions,
        platforms,
        developers,
        publishers,
        franchises,
        series,
        categories
      };
    } catch (err) {
      return err;
    }
  }
}
