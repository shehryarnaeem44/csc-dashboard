import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NewPlaylistComponent } from "./new-playlist/new-playlist.component";
import { NewPlaylistResolver } from "./new-playlist/new-playlist.resolver";

const routes: Routes = [
  {
    path: "",
    component: NewPlaylistComponent,
    resolve: { data: NewPlaylistResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistRoutingModule {}
