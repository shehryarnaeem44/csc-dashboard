import { Component, EventEmitter, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { BsDatepickerConfig, BsModalRef, BsModalService } from "ngx-bootstrap";
import { PlatformService } from "src/app/core/http-services/platform/platform.service";
import { SOMETHING_WRONG } from "src/app/shared/constants/general.constants";
import { Platform } from "src/app/shared/interfaces/platform.interface";
import { SpinnerService } from "src/app/shared/services/spinner/spinner.service";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";

@Component({
  selector: "csc-add-platform-modal",
  templateUrl: "./add-platform-modal.component.html",
  styleUrls: ["./add-platform-modal.component.scss"]
})
export class AddPlatformModalComponent implements OnInit {
  public platformForm: FormGroup;
  public isSubmitted:boolean;
  public event: EventEmitter<boolean> = new EventEmitter();
  public bsConfig: Partial<BsDatepickerConfig>;

  constructor(
    private platformService: PlatformService,
    private formBuilder: FormBuilder,
    private spinnerService:SpinnerService,
    private toasterService: ToasterService,
    private modalService: BsModalRef
  ) {
    this.isSubmitted = false;
  }

  ngOnInit() {
    this.bsConfig = {
      isAnimated: true,
      isDisabled: true,
    };
    this.buildPlatformForm();
  }

  public buildPlatformForm() {
    this.platformForm = this.formBuilder.group({
      longName: [, [Validators.required]],
      shortName: [, [Validators.required]],
      generation: [, [Validators.required]],
      manufacturer: [],
      platformReleaseDate: []
    });
  }

  public getControl(name: string): AbstractControl {
    return this.platformForm.get(name);
  }

  public async onSubmit(): Promise<void> {
    try {
      this.spinnerService.show();
      this.isSubmitted = true;
      const newPlatform:Platform = {
        shortName: this.platformForm.get('shortName').value,
        longName: this.platformForm.get('longName').value,
        generation: this.platformForm.get('generation').value,
        manufacturer: this.platformForm.get('manufacturer').value,
        releaseDate: this.platformForm.get('platformReleaseDate').value
      }
      await this.platformService.addPlatform(newPlatform);
      this.platformForm.reset();
      this.spinnerService.hide();
      this.onClose(true)
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
      this.onClose(false)
    }
  }

  triggerEvent(status: boolean) {
    this.event.emit(status || false);
  }
  
  public onClose(status?){
    this.triggerEvent(status);
    this.modalService.hide();
  }
}
