import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPlatformModalComponent } from './add-platform-modal.component';

describe('AddPlatformModalComponent', () => {
  let component: AddPlatformModalComponent;
  let fixture: ComponentFixture<AddPlatformModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPlatformModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPlatformModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
