import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlatformsRoutingModule } from './platforms-routing.module';
import { AddPlatformModalComponent } from './add-platform-modal/add-platform-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { BsDatepickerModule, DatepickerModule } from 'ngx-bootstrap';


@NgModule({
  declarations: [AddPlatformModalComponent],
  imports: [
    CommonModule,
    PlatformsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    SharedModule
  ]
})
export class PlatformsModule { }
