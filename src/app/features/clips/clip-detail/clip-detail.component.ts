import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Category } from "src/app/shared/interfaces/category.interface";
import { Clip } from "src/app/shared/interfaces/clip.interface";

@Component({
  selector: "csc-clip-detail",
  templateUrl: "./clip-detail.component.html",
  styleUrls: ["./clip-detail.component.scss"]
})
export class ClipDetailComponent implements OnInit {
  public clip: Clip;
  public categories: Category[];
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
    this.categories = [];
  }

  ngOnInit() {
    this.getClip();
  }

  public getClip() {
    const resolvedData = this.activatedRoute.snapshot.data.data;
    this.clip = resolvedData;
  }

  public onClipEdit() {
    this.router.navigateByUrl(`/clips/edit-clip/${this.clip._id}`);
  }
}
