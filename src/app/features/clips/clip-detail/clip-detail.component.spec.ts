import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClipDetailComponent } from './clip-detail.component';

describe('ClipDetailComponent', () => {
  let component: ClipDetailComponent;
  let fixture: ComponentFixture<ClipDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClipDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClipDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
