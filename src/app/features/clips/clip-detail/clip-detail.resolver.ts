import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot
} from "@angular/router";
import { ClipsService } from "src/app/core/http-services/clips/clips.service";
import { CLIP_NOT_FOUND } from "src/app/shared/constants/clips.constants";
import { Clip } from "src/app/shared/interfaces/clip.interface";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";

@Injectable({
  providedIn: "root"
})
export class ClipDetailResolver implements Resolve<any> {
  constructor(
    private clipsService: ClipsService,
    private router: Router,
    private toasterService: ToasterService,
  ) {}

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    try {
      const clipId: string = route.params.clipId;
      const clip: Clip = await this.clipsService.getClip(clipId)
      if (!clip) {
        this.router.navigateByUrl("/clips");
        this.toasterService.showError(CLIP_NOT_FOUND);
        return;
      }
      return clip
    } catch (err) {
      this.router.navigateByUrl("/clips");
      this.toasterService.showError(CLIP_NOT_FOUND);
      return err;
    }
  }
}
