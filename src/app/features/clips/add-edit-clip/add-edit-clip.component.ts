import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { CategoriesService } from "src/app/core/http-services/categories/categories.service";
import { ClipsService } from "src/app/core/http-services/clips/clips.service";
import { ADD_CLIP_SUCCESS } from "src/app/shared/constants/clips.constants";
import {
  RESOLUTION_ENUM,
  VIDEO_FORMAT_ENUM
} from "src/app/shared/constants/game.constants";
import { SOMETHING_WRONG } from "src/app/shared/constants/general.constants";
import { ADD_SOURCE_SUCCESS } from "src/app/shared/constants/sources.constants";
import { Category } from "src/app/shared/interfaces/category.interface";
import { Clip } from "src/app/shared/interfaces/clip.interface";
import { Source } from "src/app/shared/interfaces/source.interface";
import { ManageDatabaseFlowService } from "src/app/shared/services/manage-database-flow/manage-database-flow.service";
import { SpinnerService } from "src/app/shared/services/spinner/spinner.service";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";
import * as moment from 'moment';

@Component({
  selector: "csc-add-edit-clip",
  templateUrl: "./add-edit-clip.component.html",
  styleUrls: ["./add-edit-clip.component.scss"]
})
export class AddEditClipComponent implements OnInit {
  public sources: Source[];
  public categories: Category[];
  public authors: string[];
  public resolutions: string[];
  public formats: string[];
  public clipForm: FormGroup;
  public isSubmitted: boolean;
  public durationInMin:number;
  public editClip: Clip;
  videoUrl: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toasterService: ToasterService,
    private spinnerService: SpinnerService,
    private clipService: ClipsService,
    private databaseFlowService: ManageDatabaseFlowService,
    private router: Router,
    private categoryService: CategoriesService,
    private sanitizer: DomSanitizer
  ) {
    this.sources = [];
    this.categories = [];
    this.authors = [];
    this.isSubmitted = false;
    this.resolutions = RESOLUTION_ENUM;
    this.formats = VIDEO_FORMAT_ENUM;
  }

  ngOnInit() {
    this.getData();
    this.buildClipForm();
    this.addDefaultFormData();
    this.checkSourceAdded();
  }

  public getData() {
    const resolvedData = this.activatedRoute.snapshot.data.data;
    this.sources = resolvedData.sources;
    this.categories = resolvedData.categories;
    this.authors = resolvedData.authors;
    this.editClip = resolvedData.clip;
  }

  public buildClipForm() {
    this.clipForm = this.formBuilder.group({
      source: [, [Validators.required]],
      name: [, [Validators.required]],
      order: [],
      description: [, [Validators.required]],
      duration: [{value:"", disabled:true}, [Validators.required]],
      resolution: [, [Validators.required]],
      format: [, [Validators.required]],
      videoBitrate: [],
      audioBitrate: [],
      filePath: [, [Validators.required]],
      author: [, [Validators.required]],
      tags: [, [Validators.required]],
      category: [, [Validators.required]],
      excludeFromPlaylist: []
    });

    this.clipForm
      .get("tags")
      .valueChanges.toPromise()
      .then(tags => {
        this.clipForm.patchValue({ tags: tags });
      });
  }

  public getControl(name: string): AbstractControl {
    return this.clipForm.get(name);
  }

  public addDefaultFormData() {
    if (!this.editClip) {
      return;
    }

    this.clipForm.patchValue({ name: this.editClip.name });
    this.clipForm.patchValue({ order: this.editClip.order });
    this.clipForm.patchValue({ description: this.editClip.description });
    this.clipForm.patchValue({ duration: this.editClip.duration });
    this.clipForm.patchValue({ resolution: this.editClip.resolution });
    this.clipForm.patchValue({ format: this.editClip.format });
    this.clipForm.patchValue({ videoBitrate: this.editClip.videoBitrate });
    this.clipForm.patchValue({ audioBitrate: this.editClip.audioBitrate });
    this.clipForm.patchValue({ filePath: this.editClip.filePath });
    this.clipForm.patchValue({ author: this.editClip.author });
    this.clipForm.patchValue({ category: this.editClip.category._id });
    this.clipForm.patchValue({ tags: this.editClip.tags.split(",") });
  }

  public checkSourceAdded() {
    let sourceId: string;
    if (this.editClip) {
      sourceId = this.editClip.source._id;
    }

    if (
      this.databaseFlowService.source &&
      this.databaseFlowService.source._id
    ) {
      sourceId = this.databaseFlowService.source._id;
    }

    this.clipForm.patchValue({ source: sourceId });
  }

  public async onSubmit(): Promise<void> {
    try {
      this.isSubmitted = true;
      this.spinnerService.show();
      const newClip: Clip = {
        sourceId: this.clipForm.get("source").value,
        name: this.clipForm.get("name").value,
        order: this.clipForm.get("order").value,
        description: this.clipForm.get("description").value,
        duration: this.durationInMin,
        resolution: this.clipForm.get("resolution").value,
        format: this.clipForm.get("format").value,
        videoBitrate: this.clipForm.get("videoBitrate").value,
        audioBitrate: this.clipForm.get("audioBitrate").value,
        filePath: this.clipForm.get("filePath").value,
        author: this.clipForm.get("author").value,
        tags: this.clipForm.get("tags").value,
        categoryId: this.clipForm.get("category").value
      };

      console.log(this.clipForm.get("tags").value);
      const tags: string[] = this.clipForm.get("tags").value.map(tag => {
        if (tag.label) {
          return tag.label;
        } else {
          return tag;
        }
      });

      const tagString: string = tags.join(",");
      newClip.tags = tagString;
      if (this.editClip) {
        newClip._id = this.editClip._id;
        await this.clipService.editClip(newClip);
      } else {
        await this.clipService.addClip(newClip);
      }

      this.clipForm.reset();
      this.spinnerService.hide();
      this.toasterService.showSuccess(ADD_CLIP_SUCCESS);
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async onCategoryChange(category: Category) {
    try {
      if (category && category.name && !category._id) {
        this.spinnerService.show();
        await this.categoryService.createCategory(category);
        this.categories = await this.categoryService.getCategories();
        this.clipForm.get("category").reset();
        this.spinnerService.hide();
      }
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public onSourceChange(source: Source) {
    this.clipForm.patchValue({ resolution: source.resolution });
    this.clipForm.patchValue({ format: source.format });
    this.clipForm.patchValue({ videoBitrate: source.videoBitrate });
    this.clipForm.patchValue({ audioBitrate: source.audioBitrate });
  }

  public async onAddClip() {
    await this.onSubmit();
    this.router.navigateByUrl("/clips/add-clip");
  }

  readVideoUrl(event: any) {
    const files = event.target.files;
    this.clipForm.patchValue({name: files[0].name})
    if (files && files[0]) {
      this.videoUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(files[0]));
    }
  }

  getDuration(e) {
    const duration = e.target.duration;
    const formattedDuration = moment.utc(duration * 1000).format('HH:mm:ss').toString();
    const durationInMin = moment.duration(duration,'seconds').asMinutes()
    const finalDuration = Math.round((Number(durationInMin) + Number.EPSILON) * 100) / 100
    this.durationInMin = finalDuration;
    this.clipForm.patchValue({duration:formattedDuration})
  }
}
