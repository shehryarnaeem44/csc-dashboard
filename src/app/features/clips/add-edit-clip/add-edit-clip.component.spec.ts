import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditClipComponent } from './add-edit-clip.component';

describe('AddEditClipComponent', () => {
  let component: AddEditClipComponent;
  let fixture: ComponentFixture<AddEditClipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditClipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditClipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
