import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot
} from "@angular/router";
import { CategoriesService } from "src/app/core/http-services/categories/categories.service";
import { ClipsService } from "src/app/core/http-services/clips/clips.service";
import { SourcesService } from "src/app/core/http-services/sources/sources.service";
import { SOMETHING_WRONG } from "src/app/shared/constants/general.constants";
import { ManageDatabaseFlowService } from "src/app/shared/services/manage-database-flow/manage-database-flow.service";
import { RouterStateService } from "src/app/shared/services/router-state/router-state.service";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";

@Injectable({
  providedIn: "root"
})
export class AddClipResolver implements Resolve<any> {
  public constructor(
    private sourceService: SourcesService,
    private categoryService: CategoriesService,
    private toasterService: ToasterService,
    private router: Router,
    private routerStateService: RouterStateService,
    private databaseFlowService: ManageDatabaseFlowService,
    private clipService: ClipsService
  ) {}

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    try{
      const clipId: string = route.params.clipId;
      let data:any = await this.getData(clipId);
      data = this.checkForDatabaseFlow(data);
      console.log(data)
      return data;
    }catch(err){
      console.log(err)
      this.toasterService.showError(SOMETHING_WRONG)
      this.router.navigateByUrl('/clips')
    }
  }

  private getData(clipId: string) {
    const promises: any[] = [
      this.sourceService.getSources(),
      this.categoryService.getCategories(),
      this.clipService.getAuthors()
    ]

    if(clipId){
      promises.push(this.clipService.getClip(clipId))
    }
    return Promise.all(promises)
      .then(results => {
        return {
          sources: results[0].sources,
          categories: results[1],
          authors: results[2],
          clip: clipId && results.length === 4 ? results[3] : null
        };
      })
      .catch(err => {
        return err;
      });
  }

  private checkForDatabaseFlow(data: any) {
    const previousUrl = this.routerStateService.previousUrl;
    if(!previousUrl){
      return data;
    }
    if (previousUrl.includes('add-source') || previousUrl.includes('edit-source') && this.databaseFlowService.source) {
      data.source = this.databaseFlowService.source;
    }
    return data;
  }
}
