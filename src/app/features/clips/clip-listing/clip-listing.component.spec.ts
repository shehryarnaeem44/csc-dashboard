import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClipListingComponent } from './clip-listing.component';

describe('ClipListingComponent', () => {
  let component: ClipListingComponent;
  let fixture: ComponentFixture<ClipListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClipListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClipListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
