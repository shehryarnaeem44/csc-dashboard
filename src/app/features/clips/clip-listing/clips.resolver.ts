import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from "@angular/router";
import { ClipsService } from "src/app/core/http-services/clips/clips.service";
import { Clip } from "src/app/shared/interfaces/clip.interface";

@Injectable({
  providedIn: "root"
})
export class ClipsResolver implements Resolve<any> {
  public constructor(private clipsSerivce: ClipsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.clipsSerivce
      .getClips()
      .then((clipList: any) => {
        return {
          clips: clipList.clips,
          count: clipList.count
        }
      })
      .catch(err => {
        return err;
      });
  }
}
