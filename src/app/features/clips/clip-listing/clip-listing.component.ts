import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Toast } from "ngx-toastr";
import { ClipsService } from "src/app/core/http-services/clips/clips.service";
import { CLIP_DELETE_SUCCESS } from "src/app/shared/constants/clips.constants";
import { SOMETHING_WRONG } from "src/app/shared/constants/general.constants";
import { Clip } from "src/app/shared/interfaces/clip.interface";
import { SpinnerService } from "src/app/shared/services/spinner/spinner.service";
import { ToasterService } from "src/app/shared/services/toaster/toaster.service";

@Component({
  selector: "csc-clip-listing",
  templateUrl: "./clip-listing.component.html",
  styleUrls: ["./clip-listing.component.scss"]
})
export class ClipListingComponent implements OnInit {
  public clips: Clip[];
  public keyword: string
  public count: number;
  public currentPage: number;

  constructor(
    public activatedRoute: ActivatedRoute,
    private spinnerService: SpinnerService,
    private clipService: ClipsService,
    private toasterService: ToasterService
  ) {
    this.clips = [];
    this.currentPage = 1;
  }

  public ngOnInit() {
    this.getClips();
  }

  public getClips() {
    const resolvedData = this.activatedRoute.snapshot.data.data;
    this.clips = resolvedData.clips;
    this.count = resolvedData.count;
  }

  public async fetchClips(page?:number){
    const clipList = await this.clipService.getClips(); 
    this.clips = clipList.clips;
    this.count = clipList.count;
  }

  public async onDelete(clipId: string) {
    try {
      this.spinnerService.show();
      await this.clipService.deleteClip(clipId);
      await this.fetchClips();
      this.spinnerService.hide();
      this.toasterService.showSuccess(CLIP_DELETE_SUCCESS);
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async onSearch(){
    try {
      this.spinnerService.show();
      if (this.keyword !== "") {
        const clipList = await this.clipService.searchClips(this.keyword);
        this.clips = clipList.clips;
        this.count = clipList.count;
      } else {
        await this.fetchClips();
      }
      this.spinnerService.hide();
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

  public async pageChanged(event) {
    try {
      this.spinnerService.show();
      await this.fetchClips(event.page-1);
      this.spinnerService.hide();
    } catch (err) {
      this.spinnerService.hide();
      this.toasterService.showError(SOMETHING_WRONG);
    }
  }

}
