import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddClipResolver } from "./add-edit-clip/add-clip.resolver";
import { AddEditClipComponent } from "./add-edit-clip/add-edit-clip.component";
import { ClipDetailComponent } from "./clip-detail/clip-detail.component";
import { ClipListingComponent } from "./clip-listing/clip-listing.component";
import { ClipsResolver } from "./clip-listing/clips.resolver";
import { ClipDetailResolver } from "./clip-detail/clip-detail.resolver";
const routes: Routes = [
  {
    path: "",
    component: ClipListingComponent,
    resolve: { data: ClipsResolver }
  },
  {
    path: "add-clip",
    component: AddEditClipComponent,
    resolve: { data: AddClipResolver }
  },
  {
    path: "clip-detail/:clipId",
    component: ClipDetailComponent,
    resolve: { data: ClipDetailResolver }
  },
  {
    path: "edit-clip/:clipId",
    component: AddEditClipComponent,
    resolve: { data: AddClipResolver }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClipsRoutingModule {}
