import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { ClipsRoutingModule } from "./clips-routing.module";
import { ClipListingComponent } from "./clip-listing/clip-listing.component";
import { ClipDetailComponent } from "./clip-detail/clip-detail.component";
import { AddEditClipComponent } from "./add-edit-clip/add-edit-clip.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import {
  BsDropdownModule,
  PaginationModule,
  TypeaheadModule
} from "ngx-bootstrap";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { SharedModule } from "src/app/shared/shared.module";
import { NgSelectModule } from "@ng-select/ng-select";

@NgModule({
  declarations: [
    ClipListingComponent,
    ClipDetailComponent,
    AddEditClipComponent
  ],
  imports: [
    CommonModule,
    ClipsRoutingModule,
    SharedModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    BsDropdownModule.forRoot(),
    PaginationModule.forRoot(),
    TypeaheadModule.forRoot(),
    NgSelectModule
  ]
})
export class ClipsModule {}
