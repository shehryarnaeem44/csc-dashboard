import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import {
  FontAwesomeModule,
} from "@fortawesome/angular-fontawesome";
import { NgProgressModule } from "@ngx-progressbar/core";
import { NgProgressRouterModule } from "@ngx-progressbar/router";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { AddPlatformModalComponent } from "src/app/features/platforms/add-platform-modal/add-platform-modal.component";
import { SharedModule } from "src/app/shared/shared.module";
import { HeaderComponent } from "./header/header.component";
import { NavigationRoutingModule } from "./navigation-routing.module";
import { NavigationComponent } from "./navigation.component";
import { SideBarComponent } from "./side-bar/side-bar.component";
import { SidebarHeaderComponent } from "./side-bar/sidebar-header/sidebar-header.component";
import { SidebarMenuComponent } from "./side-bar/sidebar-menu/sidebar-menu.component";

@NgModule({
  declarations: [
    NavigationComponent,
    SidebarHeaderComponent,
    SidebarMenuComponent,
    SideBarComponent,
    HeaderComponent,
  ],
  imports: [
    CommonModule,
    NavigationRoutingModule,
    FontAwesomeModule,
    PerfectScrollbarModule,
    NgProgressModule,
    NgProgressRouterModule,
    SharedModule,
  ],
  entryComponents:[
    AddPlatformModalComponent
  ]
})
export class NavigationModule {}
