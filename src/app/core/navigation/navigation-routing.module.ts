import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { NavigationComponent } from "./navigation.component";

const routes: Routes = [
  {
    path: "",
    component: NavigationComponent,
    children: [
      {
        path: "home",
        loadChildren: () =>
          import("../../features/dashboard/dashboard.module").then(
            m => m.DashboardModule
          )
      },
      {
        path: "games",
        loadChildren: () =>
          import("../../features/games/games.module").then(m => m.GamesModule)
      },
      {
        path: "sources",
        loadChildren: () =>
          import("../../features/sources/sources.module").then(
            m => m.SourcesModule
          )
      },
      {
        path: "clips",
        loadChildren: () =>
          import("../../features/clips/clips.module").then(m => m.ClipsModule)
      },
      {
        path: "playlist",
        loadChildren: () =>
          import("../../features/playlist/playlist.module").then(
            m => m.PlaylistModule
          )
      },
      {
        path: "",
        redirectTo: "/home"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NavigationRoutingModule {}
