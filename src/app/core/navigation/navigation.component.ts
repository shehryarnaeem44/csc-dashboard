import { Component, OnInit } from "@angular/core";
import { ThemeOptions } from "src/app/theme-options";

@Component({
  selector: "csc-navigation",
  templateUrl: "./navigation.component.html",
  styleUrls: ["./navigation.component.scss"],
})
export class NavigationComponent implements OnInit {

  public constructor(public globals: ThemeOptions) { }

  public ngOnInit() {
  }

}
