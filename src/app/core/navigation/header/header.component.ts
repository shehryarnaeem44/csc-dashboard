import { Component, OnInit } from "@angular/core";
import { ThemeOptions } from "src/app/theme-options";

@Component({
  selector: "csc-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
  public constructor(public globals: ThemeOptions) {}

  public ngOnInit() {}

  public toggleSidebarMobileOpen() {
    this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
  }
}
