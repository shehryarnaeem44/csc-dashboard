import { animate, state, style, transition, trigger } from "@angular/animations";
import { Component, HostListener, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ThemeOptions } from "src/app/theme-options";
import { SidebarMenuService } from "./sidebar-menu.service";

@Component({
  selector: "csc-sidebar-menu",
  templateUrl: "./sidebar-menu.component.html",
  styleUrls: ["./sidebar-menu.component.scss"],
  animations: [
    trigger("slide", [
      state("up", style({ height: 0, opacity: "0" })),
      state("down", style({ height: "*", opacity: "1" })),
      transition("up <=> down", animate(200)),
    ]),
  ],
})
export class SidebarMenuComponent implements OnInit {
  public menus = [];
  public constructor(
      public globals: ThemeOptions,
      private sidebarMenuService: SidebarMenuService,
      private router: Router,
  ) {
    this.menus = [ ...sidebarMenuService.getMenuList() ];
  }

  private innerWidth: number;

  public ngOnInit() {
    const theActiveMenu = this.sidebarMenuService.getMenuItemByUrl(this.menus, this.router.url);
    if (theActiveMenu) {
      this.toggle(theActiveMenu);
    }

    this.innerWidth = window.innerWidth;
  }

  @HostListener("window:resize", ["$event"])
  public onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  public toggleSidebarMobileOpen() {
    if (this.innerWidth < 992) {
      this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
    }
  }

  public toggle(currentMenu) {
    this.menus = this.sidebarMenuService.toggleMenuItem(this.menus, currentMenu);
  }

  public getState(currentMenu) {

    if (currentMenu.active) {
      return "down";
    } else {
      return "up";
    }
  }

}
