import { Injectable } from "@angular/core";

interface MenuItem {
  title: string;
  type: string;
  badge?: {
    class: string;
    text: string;
  };
  link?: string;
  active?: boolean;
  icon?: string;
  submenus?: MenuItem[];
}

@Injectable({
  providedIn: "root"
})
export class SidebarMenuService {
  public menus: MenuItem[] = [
    {
      title: "Dashboard",
      type: "header"
    },
    {
      title: "Home",
      type: "simple",
      icon: '<i class="pe-7s-home"> </i>',
      link: "/home/"
    },
    {
      title: "Features",
      type: "header"
    },
    {
      title: "Games",
      type: "simple",
      icon: '<i class="pe-7s-joy"> </i>',
      link: "/games/"
    },
    {
      title: "Sources",
      type: "simple",
      icon: '<i class="pe-7s-file"> </i>',
      link: "/sources/"
    },
    {
      title: "Clips",
      type: "simple",
      icon: '<i class="pe-7s-video"> </i>',
      link: "/clips/"
    },
    {
      title: "Playlist",
      type: "simple",
      icon: '<i class="pe-7s-album"> </i>',
      link: "/playlist/"
    }
  ];

  public constructor() {}

  public getMenuList() {
    return this.menus;
  }

  public getMenuItemByUrl(aMenus: MenuItem[], aUrl: string): MenuItem {
    for (const theMenu of aMenus) {
      if (theMenu.link && theMenu.link === aUrl) {
        return theMenu;
      }

      if (theMenu.submenus && theMenu.submenus.length > 0) {
        const foundItem = this.getMenuItemByUrl(theMenu.submenus, aUrl);
        if (foundItem) {
          return foundItem;
        }
      }
    }

    return undefined;
  }

  public toggleMenuItem(
    aMenus: MenuItem[],
    aCurrentMenu: MenuItem
  ): MenuItem[] {
    return aMenus.map((aMenu: MenuItem) => {
      if (aMenu === aCurrentMenu) {
        aMenu.active = !aMenu.active;
      } else {
        aMenu.active = false;
      }

      if (aMenu.submenus && aMenu.submenus.length > 0) {
        aMenu.submenus = this.toggleMenuItem(aMenu.submenus, aCurrentMenu);

        if (aMenu.submenus.find(aChild => aChild.active)) {
          aMenu.active = true;
        }
      }

      return aMenu;
    });
  }
}
