import { Component, OnInit } from "@angular/core";
import { ThemeOptions } from "src/app/theme-options";

@Component({
  selector: "csc-sidebar-header",
  templateUrl: "./sidebar-header.component.html",
  styleUrls: ["./sidebar-header.component.scss"],
})
export class SidebarHeaderComponent implements OnInit {
  public constructor(public globals: ThemeOptions) {}

  public ngOnInit() {}
  public toggleSidebarMobileOpen() {
    this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
  }
}
