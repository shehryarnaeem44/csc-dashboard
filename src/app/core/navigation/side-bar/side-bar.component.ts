import { Component, OnInit } from "@angular/core";
import { ThemeOptions } from "src/app/theme-options";

@Component({
  selector: "csc-side-bar",
  templateUrl: "./side-bar.component.html",
  styleUrls: ["./side-bar.component.scss"],
})
export class SideBarComponent implements OnInit {
  public constructor(public globals: ThemeOptions) {}

  public ngOnInit() {}

  public toggleSidebarMobileOpen() {
    this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
  }
}
