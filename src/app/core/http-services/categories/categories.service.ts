import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Category } from "src/app/shared/interfaces/category.interface";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class CategoriesService {
  private url: string;

  constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/categories`;
  }

  public getCategories(page?: number): Promise<Category[]> {
    let params = new HttpParams();

    if (page) {
      params = params.set("page", page.toString());
    }
    return this._http
      .get<Category[]>(this.url, { params: params })
      .toPromise();
  }

  public createCategory(category: Category): Promise<void> {
    return this._http.post<void>(this.url, category).toPromise();
  }

  public editCategory(category: Category): Promise<void> {
    const editUrl: string = `${this.url}/${category._id}`;
    return this._http.patch<void>(editUrl, category).toPromise();
  }

  public deleteCategory(categoryId: string): Promise<void> {
    const deleteUrl: string = `${this.url}/${categoryId};`;
    return this._http.delete<void>(deleteUrl).toPromise();
  }
}
