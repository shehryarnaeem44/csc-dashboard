import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { of } from "rxjs";
import { Clip } from "src/app/shared/interfaces/clip.interface";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class ClipsService {
  private url: string;
  constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/clips`;
  }

  public getClips(page?: number): Promise<any> {
    let params: HttpParams = new HttpParams();
    if (page) {
      params = params.set("page", page.toString());
    }
    return this._http
      .get<any>(this.url, { params: params })
      .toPromise();
  }

  public addClip(newClip: Clip): Promise<void> {
    return this._http.post<void>(this.url, newClip).toPromise();
  }

  public getClip(clipId: string): Promise<Clip> {
    const detailsUrl: string = `${this.url}/${clipId}`;
    return this._http.get<Clip>(detailsUrl).toPromise();
  }

  public editClip(updatedClip: Clip): Promise<void> {
    const editUrl: string = `${this.url}/${updatedClip._id}`;
    return this._http.patch<void>(editUrl, updatedClip).toPromise();
  }

  public searchClips(keyword: string): Promise<any> {
    const searchUrl: string = `${this.url}/search`;
    let params: HttpParams = new HttpParams();
    params = params.set("keyword", keyword);
    return this._http
      .get<any>(searchUrl, { params: params })
      .toPromise();
  }

  public deleteClip(clipId: string): Promise<void> {
    const deleteUrl: string = `${this.url}/${clipId}`;
    return this._http.delete<void>(deleteUrl).toPromise();
  }

  public getAuthors(): Promise<any[]>{
    const authorUrl: string = `${this.url}/authors`;
    return this._http.get<any[]>(authorUrl).toPromise();
  }
}
