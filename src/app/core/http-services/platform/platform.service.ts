import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Platform } from "src/app/shared/interfaces/platform.interface";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class PlatformService {
  private url: string;

  public constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/platforms`;
  }

  public getPlatforms(page?: number): Promise<Platform[]> {
    let params: HttpParams = new HttpParams();
    if (page) {
      params = params.set("page", page.toString());
    }
    return this._http
      .get<Platform[]>(this.url, { params: params })
      .toPromise();
  }

  public addPlatform(newPlatform: Platform): Promise<void> {
    return this._http.post<void>(this.url, newPlatform).toPromise();
  }

  public editPlatform(updatedPlatform: Platform): Promise<void> {
    const editUrl: string = `${this.url}/${updatedPlatform._id}`;
    return this._http.patch<void>(editUrl, updatedPlatform).toPromise();
  }

  public deletePlatform(platformId: string): Promise<void> {
    const deleteUrl: string = `${this.url}/${platformId}`;
    return this._http.delete<void>(deleteUrl).toPromise();
  }
}
