import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Publisher } from "src/app/shared/interfaces/publisher.interface";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class PublishersService {
  private url: string;
  public constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/publishers`;
  }

  public getPublishers(page?: Number): Promise<Publisher[]> {
    let params: HttpParams = new HttpParams();

    if (page) {
      params = params.set("page", page.toString());
    }
    return this._http.get<Publisher[]>(this.url).toPromise();
  }

  public addPublisher(newPublisher: Publisher): Promise<void> {
    return this._http.post<void>(this.url, newPublisher).toPromise();
  }

  public editPublisher(updatedPublisher: Publisher): Promise<void> {
    const editUrl: string = `${this.url}/${updatedPublisher._id}`;
    return this._http.patch<void>(editUrl, updatedPublisher).toPromise();
  }

  public deletePublisher(publisherId: string): Promise<void> {
    const deleteUrl: string = `${this.url}/${publisherId}`;
    return this._http.delete<void>(deleteUrl).toPromise();
  }
}
