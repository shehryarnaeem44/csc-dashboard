import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Game } from "src/app/shared/interfaces/game.interface";
import { environment } from "src/environments/environment";
@Injectable({
  providedIn: "root"
})
export class GamesService {
  private url: string;
  public constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/games`;
  }

  public getGames(page?: number): Promise<any> {
    let params: HttpParams = new HttpParams();

    if (page) {
      params = params.set("page", page.toString());
    }

    return this._http
      .get<any>(this.url, { params: params })
      .toPromise();
  }

  public addGame(newGame: Game): Promise<void> {
    return this._http.post<void>(this.url, newGame).toPromise();
  }

  public getGame(gameId: string): Promise<Game> {
    const detailUrl: string = `${this.url}/${gameId}`;
    return this._http.get<Game>(detailUrl).toPromise();
  }

  public editGame(updatedGame: Game): Promise<void> {
    const editUrl: string = `${this.url}/${updatedGame._id}`;
    return this._http.patch<void>(editUrl, updatedGame).toPromise();
  }

  public searchGames(keyword: string): Promise<any> {
    const searchUrl: string = `${this.url}/search`;
    let params: HttpParams = new HttpParams();
    params = params.set("keyword", keyword);
    return this._http
      .get<any>(searchUrl, { params: params })
      .toPromise();
  }

  public deleteGame(gameId: string): Promise<void> {
    const deleteUrl: string = `${this.url}/${gameId}`;
    return this._http.delete<void>(deleteUrl).toPromise();
  }

  public getAllGames(): Promise<string[]>{
    const allGameUrl: string = `${this.url}/all`;
    return this._http.get<string[]>(allGameUrl).toPromise();
  }
}
