import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Franchise } from "src/app/shared/interfaces/franchise.interface";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class FranchiseService {
  private url: string;
  public constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/franchises`;
  }

  public getFranchises(page?: number): Promise<Franchise[]> {
    let params: HttpParams = new HttpParams();
    if (page) {
      params = params.set("page", page.toString());
    }

    return this._http
      .get<Franchise[]>(this.url, { params: params })
      .toPromise();
  }

  public addFranchise(newFranchise: Franchise): Promise<void> {
    return this._http.post<void>(this.url, newFranchise).toPromise();
  }

  public editFranchise(updatedFranchise: Franchise): Promise<void> {
    const editUrl: string = `${this.url}/${updatedFranchise._id}`;
    return this._http.patch<void>(this.url, updatedFranchise).toPromise();
  }

  public deleteFranchise(franchiseId: string): Promise<void> {
    const deleteUrl: string = `${this.url}/${franchiseId}`;
    return this._http.delete<void>(deleteUrl).toPromise();
  }
}
