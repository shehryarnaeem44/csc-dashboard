import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Series } from "src/app/shared/interfaces/series.interface";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class SeriesService {
  private url: string;

  public constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/series`;
  }

  public getSeries(page?: number): Promise<Series[]> {
    let params: HttpParams = new HttpParams();

    if (page) {
      params = params.set("page", page.toString());
    }

    return this._http
      .get<Series[]>(this.url, { params: params })
      .toPromise();
  }

  public addSeries(newSeries: Series): Promise<void> {
    return this._http.post<void>(this.url, newSeries).toPromise();
  }

  public editSeries(updatedSeries: Series): Promise<void> {
    const editUrl: string = `${this.url}/${updatedSeries._id}`;
    return this._http.patch<void>(editUrl, updatedSeries).toPromise();
  }

  public deleteSeries(seriesId: string): Promise<void> {
    const deleteUrl: string = `${this.url}/${seriesId}`;
    return this._http.delete<void>(deleteUrl).toPromise();
  }
}
