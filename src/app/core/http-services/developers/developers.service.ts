import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Developer } from "src/app/shared/interfaces/developer.interface";
import { environment } from "src/environments/environment";
@Injectable({
  providedIn: "root"
})
export class DevelopersService {
  private url: string;

  public constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/developers`;
  }

  public getDevelopers(page?: number): Promise<Developer[]> {
    let params = new HttpParams();

    if (page) {
      params = params.set("page", page.toString());
    }

    return this._http
      .get<Developer[]>(this.url, { params: params })
      .toPromise();
  }

  public addDeveloper(newDeveloper: Developer): Promise<void> {
    return this._http.post<void>(this.url, newDeveloper).toPromise();
  }

  public editDeveloper(updatedDeveloper: Developer): Promise<void> {
    const editUrl: string = `${this.url}/${updatedDeveloper._id}`;
    return this._http.patch<void>(editUrl, updatedDeveloper).toPromise();
  }

  public deleteDeveloper(developerId: string): Promise<void> {
    const deleteUrl: string = `${this.url}/${developerId}`;
    return this._http.delete<void>(deleteUrl).toPromise();
  }
}
