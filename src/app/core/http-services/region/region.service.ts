import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Region } from "src/app/shared/interfaces/region.interface";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class RegionService {
  private url: string;

  public constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/regions`;
  }

  public getRegions(page?: number): Promise<Region[]> {
    let params: HttpParams = new HttpParams();
    if (page) {
      params = params.set("page", page.toString());
    }
    return this._http
      .get<Region[]>(this.url, { params: params })
      .toPromise();
  }

  public addRegion(newRegion: Region): Promise<void> {
    return this._http.post<void>(this.url, newRegion).toPromise();
  }

  public editRegion(updatedRegion: Region): Promise<void> {
    const editUrl: string = `${this.url}/${updatedRegion._id}`;
    return this._http.patch<void>(editUrl, updatedRegion).toPromise();
  }

  public deleteRegion(regionId: string): Promise<void> {
    const deleteUrl: string = `${this.url}/${regionId}`;
    return this._http.delete<void>(deleteUrl).toPromise();
  }
}
