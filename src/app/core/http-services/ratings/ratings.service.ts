import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Rating } from "src/app/shared/interfaces/rating.interface";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class RatingsService {
  private url: string;
  constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/ratings`;
  }

  public getRatings(page?: number): Promise<Rating[]> {
    let params: HttpParams = new HttpParams();

    if (page) {
      params = params.set("page", page.toString());
    }

    return this._http
      .get<Rating[]>(this.url, { params: params })
      .toPromise();
  }

  public addRating(newRating: Rating): Promise<void> {
    return this._http.post<void>(this.url, newRating).toPromise();
  }

  public editRating(updatedRating: Rating): Promise<void> {
    const editUrl: string = `${this.url}/${updatedRating._id}`;
    return this._http.patch<void>(editUrl, updatedRating).toPromise();
  }

  public deleteRating(ratingId: string): Promise<void> {
    const deleteUrl: string = `${this.url}/${ratingId}`;
    return this._http.delete<void>(deleteUrl).toPromise();
  }
}
