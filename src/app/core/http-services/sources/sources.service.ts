import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Source } from "src/app/shared/interfaces/source.interface";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class SourcesService {
  private url: string;

  public constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/sources`;
  }

  public getSources(page?: number): Promise<any> {
    let params: HttpParams = new HttpParams();
    if (page) {
      params = params.set("page", page.toString());
    }
    return this._http
      .get<any>(this.url, { params: params })
      .toPromise();
  }

  public addSource(newSource: Source): Promise<void> {
    return this._http.post<void>(this.url, newSource).toPromise();
  }

  public getSource(sourceId: string): Promise<Source> {
    const detailUrl: string = `${this.url}/${sourceId}`;
    return this._http.get<Source>(detailUrl).toPromise();
  }

  public editSource(updatedSource: Source): Promise<void> {
    const editUrl: string = `${this.url}/${updatedSource._id}`;
    return this._http.patch<void>(editUrl, updatedSource).toPromise();
  }

  public searchSources(keyword: string): Promise<any> {
    const searchUrl: string = `${this.url}/search`;
    let params: HttpParams = new HttpParams();
    params = params.set("keyword", keyword);
    return this._http
      .get<any>(searchUrl, { params: params })
      .toPromise();
  }

  public deleteSource(sourceId: string): Promise<void> {
    const deleteUrl: string = `${this.url}/${sourceId}`;
    return this._http.delete<void>(deleteUrl).toPromise();
  }

  public getAuthors(): Promise<any[]>{
    const authorUrl: string = `${this.url}/authors`;
    return this._http.get<any[]>(authorUrl).toPromise();
  }
}
