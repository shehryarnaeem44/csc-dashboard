import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Clip } from "src/app/shared/interfaces/clip.interface";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class PlaylistService {
  private url: string;
  constructor(private _http: HttpClient) {
    this.url = `${environment.baseUrl}/playlist`;
  }

  public generatePlaylist(filters): Promise<Clip[]> {
    return this._http.post<Clip[]>(this.url, filters).toPromise();
  }

  public getXmlPlaylist(playlist): Promise<any>{
    const xmlPlaylistUrl: string = `${this.url}//convert-xml`;
    return this._http.post(xmlPlaylistUrl, playlist).toPromise();
  }
}
