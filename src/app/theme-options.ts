import { Injectable } from "@angular/core";

@Injectable()
export class ThemeOptions {

  // Header

  public headerFixed = true;
  public headerShadow = true;
  public headerTransparentBg = true;

  // Sidebar

  public toggleSidebarMobile = false;
  public sidebarBackground = "";
  public sidebarBackgroundStyle = "dark";
  public sidebarFixed = true;
  // Main content

  public contentBackground = "";
}
